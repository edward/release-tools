#!/bin/sh
#
# puprep: produce files useful when preparing for a point release
#
# (C) Copyright 2017-2018 Adam D. Barratt <adsb@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# Dependencies:
# - dak
# - devscripts (for the "bts" command)
# - libsoap-lite-perl (for bug information fetching using "bts")

export LC_ALL=C

POINTREL="$1"
SUITE="$2"
PKGARCHES="$3"
CONTENTARCHES="$4"
SKIP="$5"

MAJORVER="$(echo $POINTREL | cut -d. -f1)"
if [ "$SUITE" = "stable" ]; then
	PUSUITE=proposed-updates
else
	PUSUITE="${SUITE}-proposed-updates"
fi

CODENAME="$(dak admin suite-cfg get-value ${SUITE} codename)"
ARCH_ALL_CONTENTS="$(dak admin suite-cfg get-value ${SUITE} separate_contents_architecture_all)"

BASEDIR=/srv/release.debian.org/www/proposed-updates/
SECURITYDIR=/srv/release.debian.org/tmp/

cd ${BASEDIR}/${MAJORVER}
rm -rf ${POINTREL}/before
mkdir -p ${POINTREL}/before

cd ${POINTREL}

cat > .puprep-stamp <<EOT
Produced by puprep on $(date "+%Y-%m-%d %H:%M:%S")

Command line:
"$0" $*
EOT

cp -a /srv/mirrors/debian/dists/${SUITE}/ before
rm -rf before/${SUITE}/main/installer-*

zcat before/${SUITE}/main/source/Sources.gz > Sources-before
for arch in $(echo "$PKGARCHES" | tr "," " ")
do
	zcat before/${SUITE}/main/binary-${arch}/Packages.gz > ${arch}-before
done

if [ "${ARCH_ALL_CONTENTS}" = "True" ]; then
	CONTENTARCHES="$CONTENTARCHES,all"
fi
for arch in source $(echo "$CONTENTARCHES" | tr "," " ")
do
	zcat before/${SUITE}/main/Contents-${arch}.gz > Contents-${arch}-before
done

: > toskip.cs
: > toskip-debug.cs
if [ -n "$SKIP" ]; then
	dak ls -s ${PUSUITE} -f control-suite -S $(echo "$SKIP" | tr "," " ") | sort > toskip.cs
	dak ls -s ${PUSUITE}-debug -f control-suite -S $(echo "$SKIP" | tr "," " ") | sort > toskip-debug.cs
fi

dak control-suite -l ${SUITE} | sort > ${SUITE}.cs
dak control-suite -l ${PUSUITE} | sort > ${PUSUITE}.cs
(comm -3 ${PUSUITE}.cs toskip.cs ; cat ${SUITE}.cs) | sort > combined.cs

/srv/release.debian.org/tools/scripts/naive-heidi-dominate < combined.cs | sort > combined-dominated.cs

for FILE in removals.cs removals-debug.cs removals.${CODENAME} removals-dryrun.${CODENAME}
do
	: > ${FILE}
done

DUPLICATE_REMOVALS="$(cut -d" " -f2 ${BASEDIR}/${CODENAME}_comments/REMOVALS | sort | uniq -d)"
if [ -n "${DUPLICATE_REMOVALS}" ]
then
	echo "WARNING: the following packages have multiple removal bugs filed:" >&2
	echo ${DUPLICATE_REMOVALS} >&2
fi

for item in $(perl -pe "s/^#([0-9]*): ([^ ]*) (?:\[(.*)\] )?.*/\1_\2_\3/; s/ /_/g" ${BASEDIR}/${CODENAME}_comments/REMOVALS)
do
	bug=$(echo $item | cut -d_ -f1)
	pkg=$(echo $item | cut -d_ -f2)
	arches=$(echo $item | cut -d_ -f3- | sed -e "s/_/ /g")
	if ! grep -q "^${pkg} " "${SUITE}.cs"
	then
		echo "WARNING: package ${pkg} listed in REMOVALS appears not to exist in ${SUITE}, skipping" >&2
		continue
	fi
	if [ -n "$(dak ls -s ${CODENAME}-updates -a source -f control-suite ${pkg})" ]
	then
		echo "WARNING: package ${pkg} listed in REMOVALS appears to exist in ${CODENAME}-updates" >&2
		echo "WARNING: please ensure that the package is also removed from that suite" >&2
	fi
	if grep -q "^Package: ${pkg}$" ${SECURITYDIR}/${SUITE}-Sources-security
	then
		echo "WARNING: package ${pkg} listed in REMOVALS appears to exist in ${SUITE}-security" >&2
		echo "WARNING: please ensure a removal request has been filed!" >&2
	fi
	if [ -n "${arches}" ]
	then
		for arch in $arches
		do
			if [ -z "$(dak ls -s ${SUITE} -f control-suite -S $pkg -a $arch)" ]
			then
				echo "WARNING: package ${pkg} listed in REMOVALS appears not to exist on ${arch} in ${SUITE}, skipping" >&2
				continue 2 # break out to the package iterator
			fi
		done
		arches="$(echo "${arches}" | tr -s " " ",")"
		arches="-a ${arches}"
	fi
	for CHECK_SUITE in ${SUITE} ${PUSUITE}
	do
		dak ls -s ${CHECK_SUITE} -f control-suite -S $pkg $arches >> removals.cs
		dak ls -s ${CHECK_SUITE}-debug -f control-suite -S $pkg $arches >> removals-debug.cs
	done
	subject=$(bts status $bug fields:subject | cut -f2- | sed -e "s/^RM: .* -- //")
	if [ -n "${arches}" ]
	then
		arches=" -B ${arches}"
	fi
	printf "dak rm -s %s -R -p -d %s%s -m '%s' %s\n" "$SUITE" "$bug" "$arches" "$subject" "$pkg" >> removals.${CODENAME}
	printf "dak rm -n -s %s -R -p -d %s%s -m '%s' %s\n\n" "$SUITE" "$bug" "$arches" "$subject" "$pkg" >> removals-dryrun.${CODENAME}
	dak rm -n -s "$SUITE" -R -p -d ${bug}${arches} -m "${subject}" "$pkg" >> removals-dryrun.${CODENAME}
done

for FILE in removals.cs removals-debug.cs removals.${CODENAME}
do
	sort -o ${FILE} ${FILE}
done

comm -3 combined.cs removals.cs > combined-removals.cs

/srv/release.debian.org/tools/scripts/naive-heidi-dominate < combined-removals.cs | sort > combined-removals-dominated.cs

dak ls -s ${PUSUITE} -f control-suite -S debian-installer | sort > ${CODENAME}-r0-additions.cs

if [ "${SUITE}" = "stable" ]
then
	eval $(dak admin c db-shell)
	for PROPSUITE in testing unstable
	do
		/srv/release.debian.org/tools/scripts/suitecomp ${PUSUITE} gt ${PROPSUITE} | sort | \
			grep -F -f toskip.cs -v | grep -F -f removals.cs -v > propups.${PROPSUITE}
		/srv/release.debian.org/tools/scripts/suitecomp ${PUSUITE}-debug gt ${PROPSUITE}-debug | sort | \
			grep -F -f toskip-debug.cs -v | grep -F -f removals-debug.cs -v > propups.${PROPSUITE}-debug
	done
fi
