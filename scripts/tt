#! /usr/bin/python
## encoding: utf-8
#
# Copyright (c) 2009 Adeodato Simó (dato@net.com.org.es)
# Licensed under the terms of the MIT license.

"""Tool to manage Debian transitions.

This program helps the Release Team manage library transitions. It works
against an instance of the "wannab" database which contains, in addition,
dependency information.

Available commands are:

    * register: register a transition for tracking.
    * nmu-list: create a list of necessary Bin-NMUs for a transition.
    * intersect: list intersections between transitions. (TODO)
    * diff: show added/removed packages between testing and unstable.
    * genweb: generate summary web pages.
"""

import os
import re
import sys
import time
import urllib2
import optparse
import readline

from collections import defaultdict

import psycopg2
try:
    import jinja2
except ImportError:
    _has_jinja2 = False
else:
    _has_jinja2 = True

from psycopg2.extensions import SQL_IN, register_adapter
register_adapter(list, SQL_IN) # only registered for tuples by default

##

HTML_DIR = '/srv/buildd.debian.org/web/transitions'

BASE_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')
TEMPLATE_DIR = os.path.normpath(os.path.join(BASE_DIR, 'templates/transitions'))

##

class Program(object):

    DATABASE = 'service=wannab-test dbname=wannab0'

    def __init__(self):
        try:
            self.db = psycopg2.connect(self.DATABASE)
        except psycopg2.Error, e:
            raise Error('%s', e.message.rstrip('\r\n'))

        self.commands = {
            'register': self.cmd_register,
            'nmu-list': self.cmd_nmu_list,
            'intersect': self.cmd_intersect,
            'diff': self.cmd_diff,
            'genweb': self.cmd_genweb,
        }

    def run(self):
        self.parse_options()

        if self._cmd is None:
            print 'You need help, but I don\'t have any.'
        elif self._cmd not in self.commands:
            print 'No such command: %s' % self._cmd
        else:
            self.commands[self._cmd](self._args)

    ##

    def cmd_register(self, args):
        """Register one or more transitions for tracking.

        This command registers transitions in the database for later tracking
        or use.
        """
        cursor = self.db.cursor()

        for pkg in args:
            src = self.get_source(pkg)
            if src is None:
                self.warn('could not find a source for %s', pkg)
                continue

            cursor.execute('''SELECT source FROM transitions
                                WHERE source = %s''', (src,))

            if cursor.rowcount > 0:
                self.warn('transition %s already registered', src)
                continue

            removed, added = self.get_binary_differences(src)

            if not removed or not added:
                removed, added = \
                    self.get_binary_differences(src, right_suite='experimental')

            try:
                oldpkg = self.raw_input_with_default(
                        'Disappared package for %s?: ' % src,
                        ' '.join(removed)).strip()
                newpkg = self.raw_input_with_default(
                        'Target package for %s?: ' % src,
                        ' '.join(added)).strip()
            except (EOFError, KeyboardInterrupt):
                print '\nCancelled.'
                continue

            for pkg, suite in [(oldpkg, 'testing'), (newpkg, 'unstable')]:
                cursor.execute(
                    """SELECT package FROM suite_binaries
                         WHERE package = %s AND suite = %s""", (pkg, suite))
                if cursor.rowcount == 0:
                    self.warn('package %s does not exist in %s', pkg, suite)

            try:
                cursor.execute(
                    """INSERT INTO transitions (source, oldpkg, newpkg)
                         VALUES %s""", ((src, oldpkg, newpkg),))
            except psycopg2.Error, e:
                self.warn('could not register transition %s: %s',
                            src, str(e).rstrip('\r\n'))
            else:
                self.db.commit()

    ##

    def cmd_diff(self, args):
        """Show what packages got dropped and added in a transition.

        Example:

            % tt diff libmtp
            - libmtp7
            + libmtp8

        It accepts both source package names and binary package name as
        arguments. If a name ends with "/experimental", the differences
        against experimental (instead of unstable) will be shown.
        """
        for pkg in args:
            if pkg.endswith('/experimental'):
                pkg = pkg.split('/', 1)[0]
                right_suite = 'experimental'
            else:
                right_suite = 'unstable'

            src = self.get_source(pkg)

            if src is None:
                self.warn('could not find a source for %s', pkg)
                continue
            else:
                removed, added = self.get_binary_differences(src,
                                        right_suite=right_suite)

            for p in removed:
                print '- %s' % (p,)

            for p in added:
                print '+ %s' % (p,)

    ##

    def cmd_nmu_list(self, args):
        """Compose the list of Bin-NMUs needed for a given transition.

        This command outputs "nmu" commands suiteable for consumption by the
        "wb" tool. The implementation is conservative and only schedules
        rebuilds for packages in build_status:Installed, so it may be necessary
        to run more than once, separated by a day or two.

        XXX Since "wannab" is not live yet, the command will happily print
        already-scheduled Bin-NMUs if not run after "wannab0" has been
        updated (see wbadm's crontab).
        """
        cursor = self.db.cursor()
        source = self.get_source(args[0])
        removed, added = self.get_binary_differences(source)
        renames = self.guess_rename_mapping(removed, added)

        if not removed:
            self.error('there is no %s transition?', source)
            return

        # Calculate arches that need a Dep-Wait
        cursor.execute("""
            SELECT arch FROM suite_arches
              WHERE suite = 'unstable' AND arch != 'all'
            EXCEPT SELECT arch FROM suite_binaries
              WHERE suite = 'unstable' AND package IN %s
            EXCEPT SELECT arch FROM build_status
              WHERE suite = 'unstable' AND source = %s
                AND status = 'installed'""", (added, source))

        dep_wait_arches = set(x[0] for x in cursor)

        for source, data in sorted(self.get_bin_nmu_sources(removed).items()):
            arches = sorted(data['arches'])
            version = data['version']
            this_dw_arches = sorted(set(dep_wait_arches) & set(arches))

            if 'all' in arches:
                self.warn('package %s_%s has arch:all packages with '
                          'dependencies on the removed binaries, skipping',
                          source, version)
                continue

            if renames is not None:
                new_depends = ', '.join(sorted(renames[x]
                                          for x in data['depends']))
            else:
                new_depends = ', '.join(sorted(added))

            print 'nmu %s_%s . %s . -m "Rebuild against %s."' % \
                (source, version, ' '.join(arches), new_depends)

            if this_dw_arches:
                print 'dw %s_%s . %s . -m "%s"' % \
                    (source, version, ' '.join(this_dw_arches), new_depends)

    ##

    def cmd_intersect(self, args):
        raise NotImplementedError, 'cmd_intersect'

    ##

    def cmd_genweb(self, args):
        if args:
            raise Error('genweb takes no arguments')

        if not _has_jinja2:
            raise Error('genweb needs python-jinja2')

        cursor = self.db.cursor()
        all_transitions = {}

        for table in ['ongoing_transitions', 'partial_transitions',
                      'unstarted_transitions', 'finished_transitions']:
            cursor.execute("SELECT source FROM %s" % table)
            all_transitions[table] = sorted([ x[0] for x in cursor ])

        context = { 'now': time.strftime('%a, %d %b %Y %H:%M:%S %z') }

        for t in 'unstarted_transitions', 'finished_transitions':
            context[t] = all_transitions[t]

        context['ongoing_transitions'] = ongoing = []
        context['partial_transitions'] = partial = []

        for t in all_transitions['ongoing_transitions']:
            d = { 'name': t }
            removed = self.get_binary_differences(t)[0]

            cursor.execute(
                """SELECT DISTINCT source, arch
                     FROM binaries
                       JOIN depends USING (package, version, arch)
                       JOIN suite_binaries USING (package, version, arch)
                     WHERE suite = 'testing' AND dep IN %s""", (removed,))

            d['bin_total'] = cursor.rowcount
            d['src_total'] = len(set(x[0] for x in cursor))

            # This has a small bug, that will actually grab more packages than
            # $total when a package is in testing but only depends on $removed
            # in unstable.
            cursor.execute(
                """SELECT DISTINCT source, arch
                     FROM binaries
                       JOIN depends USING (package, version, arch)
                       JOIN suite_binaries sb USING (package, version, arch)
                       JOIN suite_sources ss USING (source)
                     WHERE sb.suite = 'unstable'
                       AND ss.suite = 'testing'
                       AND dep IN %s""", (removed,))

            d['bin_pending'] = cursor.rowcount
            d['pending_packages'] = sorted(set(x[0] for x in cursor))
            d['src_pending'] = len(d['pending_packages'])

            d['percent_done'] = (100 * (d['bin_total'] - d['bin_pending']) / d['bin_total'])
            d['pending_nmus'] = bool(self.get_bin_nmu_sources(removed))

            ongoing.append(d)

        for t in all_transitions['partial_transitions']:
            d = { 'name': t }
            removed = self.get_binary_differences(t, ood_action='include')[0]

            if not removed:
                cursor.execute(
                    """SELECT oldpkg FROM transitions
                         WHERE source = %s""", (t,))
                removed = [ x[0] for x in cursor ]
                self.warn('transition %s does not have removed packages, '
                          'using "%s" from the transitions table.', t,
                          ' '.join(removed))

            # Calculate source/arch pairs still depending on the old library
            # in testing.
            cursor.execute(
                """SELECT DISTINCT source, arch
                     FROM binaries
                       JOIN depends USING (package, version, arch)
                       JOIN suite_binaries USING (package, version, arch)
                     WHERE suite = 'testing' AND dep IN %s""",
                 (removed,))

            needed = defaultdict(set)
            pending = defaultdict(set)

            for source, arch in cursor:
                needed[source].add(arch)

            d['total'] = len(needed)

            # And source/arch pairs that depend on the old library in unstable
            cursor.execute(
                """SELECT DISTINCT source, arch
                     FROM binaries
                       JOIN depends USING (package, version, arch)
                       JOIN suite_binaries USING (package, version, arch)
                     WHERE suite = 'unstable'
                       AND source IN %s AND dep IN %s""",
                 (needed.keys(), removed))

            for source, arch in cursor:
                try:
                    needed[source].remove(arch) # Skip from "unstable_ok"
                except KeyError:
                    continue
                else:
                    pending[source].add(arch)

            d['unstable_ok'] = sorted((pkg, sorted(arches))
                                        for pkg, arches in needed.items()
                                        if arches)
            d['unstable_not_ok'] = sorted((pkg, sorted(arches))
                                            for pkg, arches in pending.items())

            partial.append(d)

        env = jinja2.Environment(trim_blocks=True,
                                 loader=jinja2.FileSystemLoader(TEMPLATE_DIR))
        env.filters['urlescape'] = urlescape_filter

        outfile = os.path.join(HTML_DIR, 'summary.html')
        try:
            tmpl = env.get_template('summary.html')
            tmpl.stream(context).dump(outfile + '.new', 'utf-8')
        except jinja2.exceptions.TemplateError, e:
            self.error('error when rendering: %s', e)
            try:
                os.unlink(outfile + '.new')
            except:
                pass
        else:
            os.rename(outfile + '.new', outfile)

    ##

    def get_source(self, pkg, suite='unstable'):
        """Find the source package for a binary package.

        If :pkg: is already a source package in the target suite,
        it is returned unmodified.
        """
        cursor = self.db.cursor()
        cursor.execute('''SELECT source FROM suite_sources
                            WHERE suite = %s AND source = %s''', (suite, pkg))

        if cursor.rowcount >= 1:
            return pkg

        cursor.execute('''SELECT DISTINCT source FROM suite_sources
                            JOIN binaries USING (source, source_version)
                            WHERE package = %s AND suite = %s''', (pkg, suite))

        sources = [ x[0] for x in cursor ]

        if len(sources) == 1:
            return sources[0]
        elif cursor.rowcount > 1:
            self.warn('binary %s provided by several source packages: %s',
                      pkg, ' '.join(sources))
            return sources[0]
        else:
            return None

    def get_binary_differences(self, source,
                               left_suite='testing', right_suite='unstable',
                               ood_action='ignore'):
        """Return package differences between two suites.

        This function returns a tuple (removed_pkgs, added_pkgs), where
        removed_pkgs is a list of packages provided by :source: in :left_suite:
        but not in :right_suite:, and added_pkgs the list of packages provided
        by :source: in :right_suite: but not in :left_suite:.

        The :non_decrufted_action: says how out of date packages (normally due
        to lack of decrufting, but may also be due to partial transitions)
        should be treated. "ignore" will behave as if they did not exist; you
        can pass "include" to take them into account.
        """
        cursor = self.db.cursor()

        if ood_action == 'ignore':
            join_expr = '(source, source_version, suite)'
        elif ood_action == 'include':
            join_expr = '(source, suite)'
        else:
            raise Error('unkown ood_action = %r', ood_action)

        cursor.execute('''
            SELECT DISTINCT package, suite
            FROM binaries
              JOIN suite_binaries USING (package, version, arch)
              JOIN suite_sources USING ''' + join_expr + '''
            WHERE suite IN (%s, %s) AND source = %s''',
            (left_suite, right_suite, source))

        rows = list(cursor)
        testing = set(row[0] for row in rows if row[1] == left_suite)
        unstable = set(row[0] for row in rows if row[1] == right_suite)

        return (sorted(testing - unstable), sorted(unstable - testing))

    def get_bin_nmu_sources(self, removed_pkgs, status_ok=['installed']):
        """Return packages that depend on "removed_pkgs" and need Bin-NMUs.

        This returns a dict in the form:

          { 'source1': { 'version': '1.1-1',
                         'arches': [ 'arch1', ... ],
                         'depends': [ 'dep1', ... ] }, ... }

        These are, for each architecture, the set of packages that are
        up-to-date and which have a binary depending on any package in
        "removed_pkgs". Only packages whose wanna-build status is in
        "status_ok" are returned.
        """
        cursor = self.db.cursor()
        cursor.execute('''
            SELECT distinct source, source_version, arch, dep
              FROM binaries
                JOIN sources USING (source, source_version)
                JOIN suite_binaries USING (package, version, arch)
                JOIN suite_sources USING (source, source_version, suite)
                JOIN depends USING (package, version, arch)
                JOIN build_status USING (source, source_version, arch, suite)
              WHERE suite = 'unstable'
                AND component != 'non-free'
                AND dep IN %s
                AND status IN %s
              ORDER BY 1, 2;''', (removed_pkgs, status_ok))

        ret = defaultdict(lambda: { 'arches': set(), 'depends': set() })

        for (source, version, arch, dep) in cursor:
            ret[source]['arches'].add(arch)
            ret[source]['depends'].add(dep)

            if 'version' not in ret[source]:
                ret[source]['version'] = version
            else:
                assert version == ret[source]['version']

        return ret

    def guess_rename_mapping(self, removed, added):
        """Return a guessed mapping { removed_pkg: added_pkg }."""

        def make_common_names(pkgs):
            """Creates a mapping { common: original } for pkgs, where
               common is a string intended to be the same for the same
               package in :removed: and :added:. The current algorithm is
               substituting each sequence of contiguous numbers with an
               uppercase letter, eg. lib32foo3bar1.0 -> libAfooBbarC.D"""
            d = {}
            regex = re.compile(r'[0-9]+')

            def repl(match):
                var[0] = chr(ord(var[0]) + 1)
                return var[0]

            for pkg in pkgs:
                var = ['@']
                new = regex.sub(repl, pkg)
                d[new] = pkg

            return d

        common_added = make_common_names(added)
        common_removed = make_common_names(removed)
        d = {}

        for stripped, removed_pkg in common_removed.iteritems():
            try:
                d[removed_pkg] = common_added[stripped]
            except KeyError:
                print >>sys.stderr, (
                    'Could not find renamed package for %s, '
                    'please make me smarter.' % (removed_pkg,))
                return None

        return d

    @staticmethod
    def raw_input_with_default(prompt, default):
        def pre_input_hook():
            readline.insert_text(default)
            readline.redisplay()

        readline.set_pre_input_hook(pre_input_hook)
        try:
            return raw_input(prompt)
        finally:
            readline.set_pre_input_hook(None)

    ##

    def parse_options(self):
        self._cmd = None
        self._args = []

        p = optparse.OptionParser('%prog [options] ACTION [args]')

        p.add_option('-v', '--verbose', action='count')
        p.add_option('-q', '--quiet',
                     action='store_const', dest='verbose', const=0)

        p.set_defaults(verbose=1)
        self._opts, self._args = p.parse_args()

        try:
            self._cmd = self._args.pop(0)
        except IndexError:
            self._cmd = None

    def log(self, level, msg, *args):
        if self._opts.verbose >= level:
            sys.stderr.write(msg % tuple(args))
            sys.stderr.flush()

    def warn(self, msg, *args):
        self.log(0, 'W: ' + msg + '\n', *args)

    def error(self, msg, *args):
        self.log(0, 'E: ' + msg + '\n', *args)

##

class Error(Exception):
    def __init__(self, msg, *args):
        super(Error, self).__init__(msg % tuple(args))

##

def urlescape_filter(value):
    if isinstance(value, basestring):
        return urllib2.quote(value)
    else:
        return [ urllib2.quote(x) for x in value ]

##

if __name__ == '__main__':
    try:
        prog = Program()
        prog.run()
    except Error, e:
        print >>sys.stderr, 'Error: %s' % (e,)
        sys.exit(1)
