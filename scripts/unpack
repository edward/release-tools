#! /usr/bin/python
## encoding: utf-8
#
# Copyright (c) 2008 Adeodato Simó (dato@net.com.org.es)
# Licensed under the terms of the MIT license.

"""Unpack binary or source packages from the archive."""

# TODO: do something graceful if unpackdir exists. Also, maybe it would be
# convenient to use different unpack dirs for binary and source? (Think, the
# case where you unpack a binary and then you want to look at the source.)

import re
import os
import sys
import optparse
import subprocess

##

MIRROR = '/srv/ftp.debian.org/mirror/ftp-master'

##

def main():
    opts, args = parse_options()

    for pkg in args:
        if opts.source:
            unpack_src_package(pkg, opts.suite)
        else:
            unpack_package(pkg, opts.suite, opts.arch)

##

def unpack_package(pkg, suite, arch):
    try:
        pkg, wanted_ver = pkg.split('=', 1)
    except ValueError:
        wanted_ver = None

    p = subprocess.Popen(['grep-archive', '%s:ALL:%s' % (suite, arch),
        '-XP', pkg, '-ns', 'Version,Filename'], stdout=subprocess.PIPE)

    version = p.stdout.readline().strip()
    filename = p.stdout.readline().strip()
    
    if not filename:
        print >>sys.stderr, 'E: package %s not found.' % pkg
    else:
        if wanted_ver is not None:
            filename = re.sub(
                    '_%s' % re.escape(version), '_%s' % wanted_ver, filename)
            version = wanted_ver

        unpackdir = '%s-%s' % (pkg, version)
        fullpath = os.path.join(MIRROR, filename)
        subprocess.Popen(['dpkg', '-x', fullpath, unpackdir]).wait()
        subprocess.Popen(['dpkg', '--control',
            fullpath, os.path.join(unpackdir, 'DEBIAN')]).wait()

        print 'Unpacked %s in %s' % (pkg, unpackdir)

##

def unpack_src_package(srcpkg, suite):
    try:
        srcpkg, wanted_ver = srcpkg.split('=', 1)
    except ValueError:
        wanted_ver = None

    p = subprocess.Popen(['grep-archive', '%s:ALL:source' % (suite,),
        '-XP', srcpkg, '-ns', 'Version,Directory,Files'], stdout=subprocess.PIPE)

    version = p.stdout.readline().strip()
    directory = p.stdout.readline().strip()

    if not directory:
        print >>sys.stderr, 'E: package %s not found.' % srcpkg
    else:
        dsc = None

        for line in p.stdout:
            m = re.search(r'\s(\S+\.dsc)$', line)
            if m:
                dsc = m.group(1)
                break

        assert dsc is not None

        if wanted_ver is not None:
            dsc = re.sub('_%s' % re.escape(version), '_%s' % wanted_ver, dsc)
            version = wanted_ver

        unpackdir = '%s-%s' % (srcpkg, version)
        subprocess.Popen(['dpkg-source', '-x',
            os.path.join(MIRROR, directory, dsc), unpackdir]).wait()
        print 'Unpacked %s source in %s' % (srcpkg, unpackdir)

##

def parse_options():
    p = optparse.OptionParser(usage='%prog [options] pkg [ pkg ... ]')

    p.add_option('-a', '--arch')
    p.add_option('-s', '--suite')
    p.add_option('--source', action='store_true')
    p.add_option('--binary', action='store_false', dest='source')

    p.set_defaults(arch='i386', suite='unstable', source=True)

    opts, args = p.parse_args()

    if not args:
        p.error('need at least one package')

    return opts, args

##

if __name__ == '__main__':
    main()
