#! /usr/bin/python
## encoding: utf-8
#
# Copyright (c) 2007 Adeodato Simó (dato@net.com.org.es)
# Licensed under the terms of the MIT license.

"""Print full stanzas from removals.txt for the removal of a package."""

# Here's a sed version of this script:
# sed -n "/^=/{h;b};H;/$pkg |/{x;p;bL};b;:L n;/^=/{p;b};p;bL" < $removals_file

import re
import sys

##

removals_file = '/srv/ftp.debian.org/web/removals-full.822'

##

def main():
    pkgs = sys.argv[1:]

    if not pkgs:
        print >>sys.stderr, 'E: need at least one package as argument.'
        sys.exit(1)

    is_start = lambda line: line.startswith("Date:")
    is_end = lambda line: len(line.strip()) == 0
    fallback_action = lambda line: True

    state = 1
    actions = Actions(pkgs)

    mealy = {
            # state: condition, action, next state
            1: [
                (is_start, actions.save_line, 2),
                (fallback_action, actions.noop, 1),
            ], 2: [
                (is_end, actions.maybe_print, 1),
                (fallback_action, actions.save_line, 2),
            ],
    }

    for line in file(removals_file):
        for condition, action, next in mealy[state]:
            if condition(line):
                state = next
                action(line)
                break

##

class Actions:
    def __init__(self, pkgs):
        self.lines = []
        self.regex = re.compile(r'^\s*(?:%s)_' %
                '|'.join(map(re.escape, pkgs)), re.M)

    def save_line(self, line):
        self.lines.append(line)

    def maybe_print(self, line):
        self.save_line(line)
        joined = ''.join(self.lines)
        if self.regex.search(joined):
            sys.stdout.write(joined)
        self.lines[:] = []

    def noop(self, line):
        pass

##

if __name__ == '__main__':
    main()
