#! /usr/bin/perl
use strict ;
use warnings ;

my $command = "cat" ;
if( $ARGV[0] eq '--for-real' ) {
    $command = '/usr/lib/sendmail -t -f noreply@release.debian.org' ;
    shift ;
}

my $suite = $ENV{SUITE}          or die '$SUITE must be set' ;
my $Debian = $ENV{DISTRIBUTION}  or die '$DISTRIBUTION must be set' ;
my $noticesTo = $ENV{NOTICES_TO} or die '$NOTICES_TO must be set' ;
my $fromEmail = $ENV{FROM_EMAIL} or die '$FROM_EMAIL must be set' ;

my @migrated ;
my @removed ;
my @toberemoved ;

chomp( my $myversion = `date -r $0 '+0.%y%m%d.%H%M'` );
my $from = "From: $fromEmail
Precedence: bulk
X-Trille: $myversion" ;

my $signature = "" ;
if( open SIGBLOCK, "<", "sigblock" ) {
    $signature = join "", <SIGBLOCK> ;
    close SIGBLOCK ;
}

my $removaldisclaimer = "
The script that generates this mail tries to extract removal
reasons from comments in the britney hint files. Those comments
were not originally meant to be machine readable, so if the
reason for removing your package seems to be nonsense, it is
probably the reporting script that got confused. Please check the
actual hints file before you complain about meaningless removals.
";

while( <> ) {
    chomp ;
    my ($p,$old,$new) = /^(\S+) (\S+) (\S+)$/  or die "Malformed input '$_'" ;
    $old =~ y/_/ / ;
    $new =~ y/_/ / ;

    my $subject = "$p $new MIGRATED to $suite" ;

    my $disclaimer = "" ;
    if( $new =~ / / ) {
	$subject = "$p REMOVED from $suite" ;
	my @reason = ("(no removal hint found)\n") ;
	if( open REASON,"<","reasons/${p}_$old" ) {
	    @reason = <REASON> ;
	    close REASON ;
	}
	push @removed, ['REMOVED',$p,$old,@reason] ;
	if( @reason ) {
	    $disclaimer = "  Hint: " . join("    ",@reason) .
		$removaldisclaimer ;
	}
    } else {
	if( open REASON,"<","reasons/${p}_${new}" ) {
	    push @toberemoved,['HINTED FOR REMOVAL',$p,$new,<REASON>] ;
	    close REASON ;
	}
	next if $old eq $new ;
	push @migrated, [$p,$new,$old] ;
    }
    print "=== Mailing notice for $p\n" ;
    open MAIL, "|-", $command or die "Cannot spawn $command for $p" ;
    my $to_line = "To: $noticesTo" ;
    $to_line .= "\nBcc: $ENV{BCCMAIL_TO}" if $ENV{BCCMAIL_TO} ;
    $to_line =~ s/%p/$p/g ;
    print MAIL "$from
Subject: $subject
X-\u$suite-Watch-Package: $p
X-\u$suite-Watch-Version: $new
$to_line

FYI: The status of the $p source package
in ${Debian}'s $suite distribution has changed.

  Previous version: $old
  Current version:  $new
$disclaimer
$signature" ;
    close MAIL or die "Mail about $p failed" ;
}

my $date = `date +'%Y-%m-%d (%A)'` ;
chomp $date ;

sub halign {
    my @flens ;
    for my $x ( @_ ) {
	next unless ref $x ;
	for my $i ( 0 .. $#$x ) {
	    my $l = length $x->[$i] ;
	    $flens[$i] = $l if $l > ($flens[$i] || 0) ;
	}
    }
    my @out ;
    for my $x ( @_ ) {
	if( ref $x ) {
	    my $l = "" ;
	    for my $i ( 0 .. $#$x - 1 ) {
		my $str = $x->[$i] ;
		$l .= "  " . $str . ' ' x ($flens[$i] - length $str) ;
	    }
	    push @out, $l . "  " . $x->[$#$x] ;
	} elsif(length $x == 1) {
	    my $l = 0 ;
	    for my $fl ( @flens ) {
		$l += $fl + 2 ;
	    }
	    push @out, $x x $l ;
	} else {
	    push @out, "  $x" ;
	}
    }
    return @out ;
}

exit 0 unless exists $ENV{SUMMARY_TO} ;

if( @migrated ) {
    print "=== Mailing migration summary\n" ;
    open MAIL, "|-", $command or die "Cannot spawn $command for summary" ;
    print MAIL "$from
Subject: \u$suite migration summary $date
X-\u$suite-Watch-Summary: migrated
To: $ENV{SUMMARY_TO}

On $date, the following source package(s) entered $suite:

",join("\n",halign(['source package','version','previous'],'-',@migrated)),"

$signature" ;
    close MAIL or die "Summary mail failed" ;
}

if( @removed ) {
    print "=== Mailing removal summary\n" ;
    open MAIL, "|-", $command
	or die "Cannot spawn $command for removal summary" ;
    print MAIL "$from
Subject: Testing removal summary $date
X-Testing-Watch-Summary: removed
To: $ENV{SUMMARY_TO}
";

    if( @removed ) {
	unshift @removed,
	"The following source package(s) have been removed from $suite:\n" ;
    }
    if( @toberemoved ) {
	push @removed,
	"The following package(s) appear to have valid removal hints,\n" .
	    "but are still in $suite for some reason:\n",
	    @toberemoved ;
    }
    for my $r ( @removed ) {
	print MAIL "\n" ;
	if( ref $r ) {
	    my ($action,$p,$v,@reason) = @$r ;
	    print MAIL "  $action: $p $v\n" ;
	    print MAIL "    $_" for @reason ;
	} else {
	    print MAIL $r ;
	}
    }

    print MAIL $removaldisclaimer, "\n", $signature ;
    close MAIL or die "Removal summary mail failed" ;
}

