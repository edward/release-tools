## vim:set noet ts=4 sw=4:
#
# debcheck.py: check installability of packages
#
# (C) Copyright 2009-2017 Adam D. Barratt <adsb@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# Dependencies (expressed in Debian packages):
#  * dose-debcheck
#  * python-debian (>= 0.1.21+nmu3)
#
# TODO:
#  * Improve error handling

import gzip
import logging
import os
import tempfile
import shutil
from io import open, TextIOWrapper
from subprocess import Popen, PIPE

from debian.debfile import DebFile
from debrelease import FileNotFoundException, locate_file, strip_epoch


class DebCheckFactory():
    def __init__(self, projectb, config, section):
        self.logger = logging.getLogger('debrelease.debcheck')
        self.logger.addHandler(logging.NullHandler())
        self.projectb = projectb
        self.dists = config.get('dak', 'dists')
        self.pool = config.get('dak', 'pool')
        self.extra_pools = config.get('dak', 'extra_pools')
        self.tempdir = tempfile.mkdtemp()
        self.section = section
        self.suite = config.get(section, 'base_suite')
        self.overlay_suite = section
        self.policy_suite = config.get(section, 'policy_suite', fallback=None)
        self.queue_directory = config.get(section, 'directory')
        self.use_overlay = config.getboolean(section, 'check_overlay_installability', fallback='yes')
        self.debcheck_output = config.get(section, 'debcheck_output')
        if not os.path.exists(self.debcheck_output):
            os.mkdir(self.debcheck_output)
        assert os.path.isdir(self.debcheck_output)

    def __del__(self):
        shutil.rmtree(self.tempdir)

    def check_packages(self, srcpkg, version, srcver, arch, debs=None):

        return DebCheck(
            srcpkg=srcpkg,
            version=version,
            srcver=srcver,
            projectb=self.projectb,
            dists=self.dists,
            pool=self.pool,
            extra_pools=self.extra_pools,
            queue_directory=self.queue_directory,
            arch=arch,
            debs=debs,
            suite=self.suite,
            overlay_suite=self.overlay_suite,
            policy_suite=self.policy_suite,
            use_overlay=self.use_overlay,
            temp_directory=self.tempdir,
            debcheck_output=self.debcheck_output
        )

    def filename(self, srcpkg, version, arch):
        filever = strip_epoch(version)
        return "%s/%s_%s_%s.debcheck" % (self.debcheck_output,
                                         srcpkg,
                                         filever,
                                         arch,
                                         )


class CheckFailedException(Exception):
    def __init__(self, srcpkg, version, arch, error):
        self.srcpkg, self.version, self.arch, self.error = \
            srcpkg, version, arch, error

    def __str__(self):
        return "DebCheck(%s, %s, %s) failed:\n%s" % (self.srcpkg,
                                                     self.version,
                                                     self.arch,
                                                     self.error,
                                                     )


class DebCheck():
    def __init__(self, **kwargs):
        for var in ['dists', 'arch', 'debs', 'suite', 'projectb',
                    'overlay_suite', 'queue_directory', 'temp_directory',
                    'debcheck_output', 'srcpkg', 'version', 'pool']:
            assert var in kwargs

        self.logger = logging.getLogger('debrelease.debcheck')
        self.logger.addHandler(logging.NullHandler())
        self.projectb = kwargs['projectb']
        self.dists = kwargs['dists']
        self.pool = kwargs['pool']
        if 'extra_pools' in kwargs:
            self.extra_pools = kwargs['extra_pools'].split(' ')
        else:
            self.extra_pools = []
        self.arch = kwargs['arch']
        self.suite = kwargs['suite']
        self.overlay_suite = kwargs['overlay_suite']
        if 'policy_suite' in kwargs:
            self.policy_suite = kwargs['policy_suite']
        else:
            self.policy_suite = None
        self.queue_directory = kwargs['queue_directory']
        self.temp_directory = kwargs['temp_directory']
        self.debcheck_output = kwargs['debcheck_output']
        self.debs = []
        debs = kwargs['debs']
        if 'use_overlay' in kwargs:
            self.use_overlay = kwargs['use_overlay']
        else:
            self.use_overlay = True
        self.srcpkg = kwargs['srcpkg']
        self.version = kwargs['version']
        self.srcver = kwargs.get('srcver', self.version)
        self.packageinfo = {}
        self.tocheck = {}
        self.__output = []
        self.components = {}

        if debs is None:
            self.logger.debug("Determining binary packages for %s_%s/%s",
                              self.srcpkg, self.srcver, self.arch)
            suite = None
            if self.policy_suite is not None and \
               self.srcver == self.projectb.get_version(self.policy_suite, self.srcpkg, epoch=True):
                suite = self.policy_suite
            elif self.srcver == self.projectb.get_version(self.overlay_suite, self.srcpkg, epoch=True):
                suite = self.overlay_suite
            elif self.srcver == self.projectb.get_version(self.suite, self.srcpkg, epoch=True):
                suite = self.suite
            if suite is not None:
                debug_suite = "%s-debug" % (suite, )
            debs = frozenset((pkg.filename for pkg in
                              self.projectb.get_binaries_for_version(self.srcpkg, self.srcver)
                              if pkg.arch_string in [self.arch, 'all'] and (suite is None or pkg.suite in [suite, debug_suite])))

        # confirm that any supplied files exist
        # also store the absolute path name now to avoid
        # needlessly recomputing it later on
        if debs is not None:
            self.logger.debug("Locating binary packages for %s_%s/%s",
                              self.srcpkg, self.srcver, self.arch)
            for file in debs:
                file = locate_file(file, self.queue_directory, [self.pool] + self.extra_pools)
                if not os.path.exists(file):
                    raise FileNotFoundException(file)
                self.debs.append(file)

    def _checkinstallability(self):
        self.logger.info("Running installability check for %s_%s/%s",
                         self.srcpkg, self.srcver, self.arch)
        self._generatepackagesfile()
        for component in self.components:
            if self.components[component]['packages'] == {}:
                continue
            pkglist = self.packageinfo[component]
            packagesfile = '%s/Packages' % (self.temp_directory)
            with open(packagesfile, 'w', encoding='utf-8') as f:
                f.write('\n'.join(pkglist))
                if component != 'main':
                    f.write('\n'.join(self.packageinfo['main']))
                    if component == 'contrib':
                        f.write('\n'.join(self.packageinfo['non-free']))
                    if component == 'non-free':
                        f.write('\n'.join(self.packageinfo['contrib']))
            extrapackagesfile = '%s/ExtraPackages' % (self.temp_directory)
            with open(extrapackagesfile, 'w', encoding='utf-8') as x:
                if component != 'main':
                    x.write('\n'.join(self.tocheck['main']))
                    if component == 'contrib':
                        x.write('\n'.join(self.tocheck['non-free']))
                    if component == 'non-free':
                        x.write('\n'.join(self.tocheck['contrib']))
            tocheckpackagesfile = '%s/ToCheckPackages' % (self.temp_directory)
            with open(tocheckpackagesfile, 'w', encoding='utf-8') as f:
                f.write("\n".join(self.tocheck[component]))
            params = ['dose-debcheck', '--quiet', '--failures', '--explain',
                      '--fg', tocheckpackagesfile,
                      '--latest=1', '--bg', packagesfile]
            if os.path.getsize(extrapackagesfile) > 0:
                params.extend(['--fg', extrapackagesfile])
            self.logger.debug("Command line:")
            self.logger.debug(params)
            debcheck = Popen(params,
                             stdin=PIPE,
                             stdout=PIPE,
                             stderr=PIPE,
                             close_fds=True
                             )
            (stdout, stderr) = debcheck.communicate()
            if debcheck.returncode not in [0, 1]:
                # 0 - no problems
                # 1 - installability problems found
                # other exit codes indicate failure
                raise CheckFailedException(self.srcpkg,
                                           self.version,
                                           self.arch,
                                           stderr,
                                           )
            if debcheck.returncode == 1:
                # at least one installability problem found
                self.__output.append("\n%s\n" % stdout.decode('utf-8'))

        return "\n".join(self.__output)

    def _generatepackagesfile(self):
        self._processpackages()
        components = set(self.components)
        if 'main' not in components:
            components.add('main')
        if 'non-free' in components and 'contrib' not in components:
            components.add('contrib')
        if 'contrib' in components and 'non-free' not in components:
            components.add('non-free')
        for component in components:
            self._processpackagesfile(self.suite, component)
            if self.use_overlay:
                self._processpackagesfile(self.overlay_suite, component)

    def _processpackagesfile(self, suite, component):
        if self.arch == 'all':
            # This is a bit of a hack, but required as there isn't a
            # binary-all package file
            binaryarch = 'i386'
        else:
            binaryarch = self.arch
        packagesfilename = "%s/%s/%s/binary-%s/Packages.gz" % \
            (self.dists, suite, component, binaryarch)
        if os.path.isfile(packagesfilename):
            if component not in self.components:
                self.components[component] = {}
                self.components[component]['packages'] = {}
                self.packageinfo[component] = []
                self.tocheck[component] = []
            with TextIOWrapper(gzip.open(packagesfilename),
                               encoding='utf-8') as packagesfile:
                packagelist = list(map(str.rstrip, packagesfile))
            self.packageinfo[component].extend(packagelist)

    def _processpackages(self):
        if self.debs is not None:
            self._addpackages(self.debs)

    def _addpackages(self, packages):
        for package in packages:
            self._addpackageinfo(package)

    def _addpackageinfo(self, filename):
        if filename.endswith('.udeb'):
            return
        filename = locate_file(filename, self.queue_directory, [self.pool] + self.extra_pools)
        self.logger.debug("Adding package information for %s", filename)
        debfileobj = DebFile(filename)
        parsed = debfileobj.debcontrol()
        control = debfileobj.control.get_content('control', encoding='utf-8')
        section = parsed['Section']
        if section.find('/') != -1:
            component, section = section.split('/')
        else:
            component = 'main'
        if component not in self.components:
            self.components[component] = {}
            self.components[component]['packages'] = {}
            self.packageinfo[component] = []
            self.tocheck[component] = []
        self.tocheck[component].extend(control.splitlines())
        # Add a separator line between the stanzas
        self.tocheck[component].append('')
        package = parsed['Package']
        self.components[component]['packages'][package] = {}
        self.components[component]['packages'][package]['name'] = package
        self.components[component]['packages'][package]['version'] = parsed['Version']

    def __str__(self):
        return self.output

    def write_to_file(self, f):
        f.write(self.output)

    def create_output_file(self):
        with open(self.filename, "w", encoding='utf-8') as f:
            self.write_to_file(f)

    @property
    def filename(self):
        return "%s/%s_%s_%s.debcheck" % (self.debcheck_output,
                                         self.srcpkg,
                                         strip_epoch(self.version),
                                         self.arch,
                                         )

    @property
    def output(self):
        if not hasattr(self, '_output'):
            self._output = self._checkinstallability()
        return self._output
