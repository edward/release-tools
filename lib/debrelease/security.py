## vim:set et ts=4 sw=4:
#
# security.py: read DSA information from a pregenerated DB and create
#              comments files
#
# (C) Copyright 2009 Philipp Kern <pkern@debian.org>
# (C) Copyright 2009-2019 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.

from debrelease import safe_pickle_load


class DSAFactory():
    def __init__(self, config):
        self.dsa_db = config.get('security', 'dsa_db')
        with open(self.dsa_db, 'rb') as f:
            self.dsa_list = safe_pickle_load(f)

    def get_dsa(self, srcpkg, version):
        if srcpkg not in self.dsa_list:
            return None
        dsas_for_srcpkg = self.dsa_list[srcpkg]
        if version in dsas_for_srcpkg:
            entry = dsas_for_srcpkg[version]
            description = entry['description']
            if description.startswith('-'):
                description = ' ' + description
            if '-' in description:
                description = description.split(' - ')[1]
                return {'srcpkg': srcpkg,
                        'version': version,
                        'advisory': entry['advisory'],
                        'date': entry['date'],
                        'description': description}
            else:
                return None
        return None
