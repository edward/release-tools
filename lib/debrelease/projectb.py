## vim:set et ts=4 sw=4 cindent:
#
# projectb.py: wrap access to the ProjectB database for the Release Team tools
#
# (C) 2007-2012 Philipp Kern <pkern@debian.org>
# Copyright 2009-2017 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# Dependencies (expressed in Debian packages):
#  * python-psycopg2 (for projectb access)

import logging
import re

from collections import defaultdict
from debian.debian_support import Version

import psycopg2 as pg
import psycopg2.extras


class ProjectB():
    def __init__(self):
        self.logger = logging.getLogger('debrelease.projectb')
        self.logger.addHandler(logging.NullHandler())
        self.logger.debug('ProjectB: __init__')

        self.conn = pg.connect('service=projectb')
        self.conn.set_client_encoding('UTF8')
        self.conn.set_session(readonly=True, autocommit=True)
        self.cache = defaultdict(set)
        self.neg_cache = defaultdict(set)

    def get_connection(self):
        return self.conn

    package_info_query = \
        """SELECT DISTINCT ON (binpkg, arch_string, suite)
               CASE
                   WHEN architecture_is_source THEN ''
                   ELSE tmp.package
               END AS binpkg,
               CASE
                   WHEN architecture_is_source THEN ''
                   ELSE tmp.type
               END AS bintype,
               CASE
                   WHEN architecture_is_source THEN ''
                   ELSE tmp.version
               END AS fullbinver,
               CASE
                   WHEN architecture_is_source THEN ''
                   ELSE SUBSTRING(tmp.version FROM POSITION(':' IN tmp.version) + 1)
               END AS binver,
               tmp.value AS built_using,
               CASE
                   WHEN architecture_is_source THEN ''
                   ELSE tmp.architecture
               END AS arch_string,
               CASE
                   WHEN architecture_is_source THEN ''
                   ELSE (component.name || '/'::text) || files.filename
               END AS filename,
               tmp.source,
               tmp.source_version AS fullsourcever,
               suite.suite_name AS suite,
               SUBSTRING(tmp.source_version FROM POSITION(':' IN tmp.source_version) + 1) AS sourcever
        FROM ( SELECT s.source AS package,
                      s.version,
                      s.source,
                      s.id source_id,
                      s.version AS source_version,
                      sa.suite AS suite_id,
                      true AS architecture_is_source,
                      'source'::text AS architecture,
                      'dsc'::text AS type,
                      sc.component_id,
                      '' as value,
                      s.file
               FROM source s
                 JOIN src_associations sa ON s.id = sa.source
                 JOIN source_component sc ON s.id = sc.source_id AND sa.suite = sc.suite_id
               UNION
               SELECT b.package,
                   b.version,
                   s.source,
                   s.id source_id,
                   s.version AS source_version,
                   ba.suite AS suite_id,
                   false AS architecture_is_source,
                   a.arch_string AS architecture,
                   b.type,
                   bc.component_id,
                   COALESCE(bm.value, '') AS value,
                   b.file
              FROM binaries b
              JOIN source s ON b.source = s.id
              JOIN architecture a ON b.architecture = a.id
              JOIN bin_associations ba ON b.id = ba.bin
              JOIN binary_component bc ON b.id = bc.binary_id AND ba.suite = bc.suite_id
              LEFT JOIN (
                          binaries_metadata bm
                          INNER JOIN metadata_keys mk ON mk.key_id = bm.key_id
                              AND mk.key = 'Built-Using'
                        ) ON b.id = bm.bin_id
        ) tmp
        JOIN suite ON tmp.suite_id = suite.id
        JOIN archive ON suite.archive_id = archive.id
        JOIN component ON tmp.component_id = component.id
        JOIN files ON tmp.file = files.id
        WHERE
            (NOT (architecture_is_source AND EXISTS
                (SELECT 1
                 FROM bin_associations_binaries bab JOIN source s ON bab.source = s.id
                 WHERE tmp.source_id = bab.source
                       AND s.version = tmp.source_version
                       AND bab.suite = tmp.suite_id
                 )
            ))
            -- optional further filtering
             %s %s %s
        ORDER BY binpkg, arch_string, suite, fullbinver DESC
    ;"""

    def ensure_cached(self, package, suite, source=True):
        self.logger.debug("projectb::ensure_cached: %s %s %s", package, suite, source)
        if source:
            packages = [package]
        else:
            if 'bin:%s' % (package) in self.neg_cache[suite]:
                packages = []
            elif not any(item for item in self.cache[suite] if item.binpkg == package):
                packages = self.get_source_pkgs(suite, package)
                if not packages:
                    self.logger.debug("caching bin:%s", package)
                    query = self.package_info_query % (
                        'AND tmp.package = \'%s\'' % (package),
                        'AND suite.suite_name IN (\'%s\', \'%s-debug\')' % (suite, suite),
                        '')
                    cursor = self.conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
                    cursor.execute(query)
                    result = cursor.fetchall()
                    self.cache[suite] |= set(result)
                    if not result:
                        self.neg_cache[suite].add('bin:%s' % (package))
            else:
                packages = []
        for this_package in packages:
            if 'src:%s' % (this_package) in self.neg_cache[suite]:
                continue
            if not any(item for item in self.cache[suite] if item.source == this_package):
                self.logger.debug("caching %s", this_package)
                query = self.package_info_query % (
                    'AND source=\'%s\'' % (this_package),
                    'AND suite.suite_name IN (\'%s\', \'%s-debug\')' % (suite, suite),
                    '')
                cursor = self.conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
                cursor.execute(query)
                result = cursor.fetchall()
                self.cache[suite] |= set(result)
                if not result:
                    self.neg_cache[suite].add('src:%s' % (this_package))

    suite_comparison_query = \
        """
            SELECT
                '%s' left_suite,
                lp package,
                la architecture,
                lv left_version,
                '%s'::text op,
                rv right_version,
                '%s' right_suite
            FROM
            (
                SELECT
                    lp::text,
                    lv::text,
                    la::text,
                    rp::text,
                    rv::text,
                    ra::text,
                    1 AS sortorder
                FROM
                (
                    SELECT
                        package lp,
                        b.version lv,
                        arch_string la
                    FROM
                        architecture a
                        JOIN binaries b ON b.architecture = a.id
                        JOIN bin_associations ON b.id = bin_associations.bin
                        JOIN suite ON suite.id = bin_associations.suite
                    WHERE
                        suite_name = '%s'
                ) AS left_suite
                INNER JOIN (
                    SELECT
                        package rp,
                        MAX(b.version) AS rv,
                        arch_string ra
                    FROM
                        architecture a
                        JOIN binaries b ON b.architecture = a.id
                        JOIN bin_associations ON b.id = bin_associations.bin
                        JOIN suite ON suite.id = bin_associations.suite
                    WHERE
                        suite_name = '%s'
                    GROUP BY
                        rp,
                        ra
                ) AS right_suite ON lp = rp AND la = ra
                WHERE
                    lv %s rv
                UNION
                SELECT
                    lp::text,
                    lv::text,
                    'source' la,
                    rp::text,
                    rv::text,
                    'source' ra,
                    0 AS sortorder
                FROM
                (
                    SELECT
                        source lp,
                        version lv
                    FROM
                        source_suite
                    WHERE
                        suite_name = '%s'
                ) AS left_suite
                INNER JOIN (
                    SELECT
                        source rp,
                        MAX(version) rv
                    FROM
                        source_suite
                    WHERE
                        suite_name = '%s'
                    GROUP BY
                        rp
                ) AS right_suite ON lp = rp
                 WHERE lv %s rv
            ) packages
            ORDER BY
                lp,
                lv,
                sortorder,
                la
            ;
            """

    def compare_suites_op(self, left_suite, operator, right_suite):
        self.logger.debug('projectb::compare_suites_op: %s %s %s', left_suite, operator, right_suite)
        query = self.suite_comparison_query % (
            left_suite,
            operator,
            right_suite,
            left_suite,
            right_suite,
            operator,
            left_suite,
            right_suite,
            operator,
        )
        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def get_architectures(self, suite, srcpkg, version=None, binversion=False):
        self.logger.debug("projectb::get_architectures: %s %s %s %s", suite, srcpkg, version, binversion)
        self.ensure_cached(srcpkg, suite)
        return frozenset(item.arch_string for item in self.cache[suite] if item.source == srcpkg and (
            version is None
            or (
                (binversion and item.binver == version)
                or
                (not binversion and item.sourcever == version)
            )
        ))

    version_query = \
        """select source.version from source join src_associations on source.id
        = src_associations.source join suite on src_associations.suite =
        suite.id where source.source = '%s' and  suite.suite_name = '%s'
        order by source.version desc"""

    def get_version(self, suite, srcpkg, expected=False, epoch=False):
        self.logger.debug("projectb::get_version %s %s %s %s", suite, srcpkg, expected, epoch)
        self.ensure_cached(srcpkg, suite)
        if epoch:
            result = [item.fullsourcever for item in self.cache[suite]
                      if item.source == srcpkg]
        else:
            result = [item.sourcever for item in self.cache[suite]
                      if item.source == srcpkg]
        if expected:
            assert result
        if result:
            return max(result, key=Version)
        else:
            return None

    suite_architectures_query = \
        """select a.arch_string
           from architecture a
           inner join suite_architectures sa on a.id = sa.architecture
           inner join suite s on sa.suite = s.id
           where s.suite_name = '%s'"""

    def get_suite_architectures(self, suite):
        self.logger.debug("projectb: get_suite_architectures %s", suite)
        query = self.suite_architectures_query % (suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return frozenset((row[0] for row in result))

    # XXX: This might get wrong results due to messed up dsc/source overrides
    component_query = \
        """select component.name from component join override on
        override.component = component.id join override_type on override_type.id
        = override.type join suite on override.suite = suite.id where
        override.package = '%s' and suite.suite_name = '%s'
        and override_type.type = '%s'"""

    def get_component(self, suite, srcpkg, expected=False):
        self.logger.debug("projectb::get_component %s %s %s", suite, srcpkg, expected)
        query = self.component_query % (srcpkg, suite, 'dsc')
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if expected:
            assert result
        if result:
            return result[0][0]
        else:
            return None

    dsc_file_query = \
        """
            SELECT (component.name || '/'::text) || files.filename AS filename
            FROM source
            INNER JOIN dsc_files ON dsc_files.source = source.id
            INNER JOIN files ON dsc_files.file = files.id
            INNER JOIN files_archive_map fam ON files.id = fam.file_id
            INNER JOIN component ON fam.component_id = component.id
            INNER JOIN archive ON fam.archive_id = archive.id
            WHERE archive.name IN ('ftp-master', 'debian-debug', 'policy') AND
              files.filename like '%%.dsc' AND
              source.source = '%s' AND source.version = '%s'
        """

    def get_dsc_file(self, srcpkg, version):
        self.logger.debug("projectb::get_dsc_file %s_%s", srcpkg, version)
        if re.search(r'\+b\d+$', version):
            raise ValueError('Cannot fetch dsc for binNMUs (%s_%s)'
                             % (srcpkg, version))
        query = self.dsc_file_query % (srcpkg, version)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result[0][0]

    pool_file_query = \
        """select (component.name || '/'::text) || files.filename as filename
           from files join files_archive_map fam on files.id = fam.file_id
           join component on fam.component_id = component.id
           join archive on fam.archive_id = archive.id
           where archive.name in ('ftp-master', 'debian-debug', 'policy') and
           (filename like '%%/%s' or filename='%s')"""

    def get_pool_file(self, filename):
        self.logger.debug("projectb::get_pool_file %s", filename)
        query = self.pool_file_query % (filename)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result[0][0]

    def get_current_suite_version(self, suite):
        self.logger.debug("projectb::get_current_suite_version %s", suite)
        query = "select version from suite where suite_name = '%s'" \
            % (suite,)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result[0][0]

    def get_suite_codename(self, suite):
        self.logger.debug("projectb::get_suite_codename %s", suite)
        query = "select codename from suite where suite_name = '%s'" \
            % (suite,)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result[0][0]

    def get_binaries(self, suite, srcpkg, arch=None, version=None):
        self.logger.debug("projectb::get_binaries %s %s %s %s", suite, srcpkg, arch, version)
        self.ensure_cached(srcpkg, suite)
        return frozenset(item.binpkg for item in self.cache[suite] if item.source == srcpkg and
                         item.binpkg and
                         arch in [None, item.arch_string] and version in [None, item.fullsourcever])

    def get_built_using(self, suite, binpkg, arch, version=None, epoch=False):
        self.logger.debug("projectb::get_built_using %s %s %s %s", suite, binpkg, arch, version)
        self.ensure_cached(binpkg, suite)
        if epoch:
            items = [item.built_using.split(",") for item in self.cache[suite]
                    if item.binpkg == binpkg
                    and arch == item.arch_string and version in [None, item.fullbinver]]
            if items:
                return items[0]
            else:
                return []
        else:
            items = [item.built_using.split(",") for item in self.cache[suite]
                    if item.binpkg == binpkg
                    and arch == item.arch_string and version in [None, item.binver]]
            if items:
                return items[0]
            else:
                return []

    def get_binaries_for_version(self, srcpkg, version):
        self.logger.debug("projectb::get_binaries_for_version %s_%s", srcpkg, version)
        query = self.package_info_query % (
            'AND source = \'%s\'' % (srcpkg),
            'AND source_version = \'%s\'' % (version),
            '')
        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def get_binary_version(self, suite, binpkg, arch, epoch=False):
        self.logger.debug("projectb::get_binary_version %s %s/%s %s", suite, binpkg, arch, epoch)
        self.ensure_cached(binpkg, suite, source=False)
        if epoch:
            result = [item.fullbinver for item in self.cache[suite]
                      if item.binpkg == binpkg and item.arch_string == arch]
        else:
            result = [item.binver for item in self.cache[suite]
                      if item.binpkg == binpkg and item.arch_string == arch]
        if result:
            return max(result, key=Version)
        else:
            return None

    def get_binary_filename(self, suite, srcpkg, binpkg, arch):
        self.logger.debug("projectb::get_binary_filename %s %s %s %s", suite, srcpkg, binpkg, arch)
        self.ensure_cached(srcpkg, suite)
        result = {item.fullbinver: item.filename for item in self.cache[suite]
                  if item.binpkg == binpkg and item.source == srcpkg and item.arch_string == arch}
        if result:
            return result[max(result, key=Version)]
        else:
            return None

    is_source_pkg_query = \
        """select count(*) from source join src_associations on source.id
        = src_associations.source join suite on src_associations.suite =
        suite.id where source.source = '%s' and suite.suite_name = '%s'"""

    def is_source_pkg(self, suite, pkgname):
        self.logger.debug("projectb::is_source_pkg %s %s", suite, pkgname)
        query = self.is_source_pkg_query % (pkgname, suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if result[0][0] > 0:
            return True

        return False

    get_source_pkg_query = \
        """SELECT DISTINCT source.source FROM binaries
            JOIN source ON binaries.source = source.id
            JOIN src_associations ON source.id = src_associations.source
            JOIN suite ON src_associations.suite = suite.id
           WHERE binaries.package = '%s'
             AND suite.suite_name = '%s'"""

    def get_source_pkgs(self, suite, pkgname):
        self.logger.debug("projectb::get_source_pkgs %s %s", suite, pkgname)
        query = self.get_source_pkg_query % (pkgname, suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if result:
            return [item[0] for item in result]

        return []

    def get_source_pkg(self, suite, pkgname):
        self.logger.debug("projectb::get_source_pkg %s %s", suite, pkgname)
        result = self.get_source_pkgs(suite, pkgname)
        if result:
            return result[0]

        return None

    def has_udebs(self, suite, srcpkg):
        self.logger.debug("projectb::has_udebs %s %s", suite, srcpkg)
        self.ensure_cached(srcpkg, suite)
        return any(item for item in self.cache[suite] if item.source == srcpkg and item.bintype == 'udeb')

    def is_udeb_only(self, suite, srcpkg):
        self.logger.debug("projectb::is_udeb_only %s %s", suite, srcpkg)
        self.ensure_cached(srcpkg, suite)
        binaries = [item for item in self.cache[suite] if item.source == srcpkg]
        return binaries and all(item.bintype == 'udeb' for item in binaries)

    changelog_query = \
        """SELECT changelog FROM changelogs
           WHERE source = '%s'
             AND version = '%s'"""

    def get_changelog(self, srcpkg, version):
        self.logger.debug("projectb::get_changelog %s_%s", srcpkg, version)
        query = self.changelog_query % (srcpkg, version)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if result:
            return result[0][0]

        return ''

    changelog_dist_query = \
        """SELECT STRING_AGG(DISTINCT distribution, ', ')
           FROM changelogs
           WHERE SPLIT_PART(source, ' ', 1) = '%s'
             AND version = '%s'"""

    def get_changelog_dist(self, srcpkg, version):
        self.logger.debug("projectb::get_changelog dist %s_%s", srcpkg, version)
        query = self.changelog_dist_query % (srcpkg, version)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if result:
            return result[0][0]

        return ''

    BUGS_CLOSED_RE = re.compile(
        r'closes:\s*(?:bug)?\#?\s?\d+(?:,\s*(?:bug)?\#?\s?\d+)*',
        re.IGNORECASE | re.DOTALL)

    def get_bugs_closed(self, srcpkg, version):
        self.logger.debug("projectb::get_bugs_closed %s_%s", srcpkg, version)
        changelog = self.get_changelog(srcpkg, version)
        bugs = set()
        for m in self.BUGS_CLOSED_RE.findall(changelog):
            bugs.update(re.findall(r'\d+', m))
        return frozenset(bugs)

    changes_files_for_policy_queue_query = \
        """SELECT changes.changesname, changes.id
           FROM changes
           INNER JOIN policy_queue_upload uploads ON changes.id=uploads.changes_id
           INNER JOIN policy_queue queue on uploads.policy_queue_id = queue.id
           WHERE queue_name = '%s'
        """

    def get_changes_files_for_policy_queue(self, suite):
        self.logger.debug("projectb::get_changes_files_for_policy_queue %s", suite)
        query = self.changes_files_for_policy_queue_query % (suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return frozenset(((row[0], row[1]) for row in result))

    changes_details_query = \
        """ SELECT changesname, source, binaries, architecture, version,
               distribution, urgency, maintainer, fingerprint, changedby,
               date, closes, seen
            FROM changes
            WHERE id=%s
        """

    def get_changes_details(self, changesid):
        self.logger.debug("projectb::get_changes_details %s", changesid)
        query = self.changes_details_query % (changesid)
        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
        cursor.execute(query)
        result = cursor.fetchall()
        if result:
            return result[0]
        else:
            return None

    # This may give incorrect results if the same file is in
    # different components in different archives
    policy_binaries_from_changesid_query = \
        """
            SELECT binaries.package, binaries.version, arch_string architecture,
              (component.name || '/'::text) || files.filename
            FROM changes
            INNER JOIN policy_queue_upload uploads ON changes.id=uploads.changes_id
            INNER JOIN policy_queue_upload_binaries_map binmap ON uploads.id = binmap.policy_queue_upload_id
            INNER JOIN binaries ON binmap.binary_id = binaries.id
            INNER JOIN architecture ON binaries.architecture = architecture.id
            INNER JOIN source ON binaries.source = source.id
            INNER JOIN files ON binaries.file = files.id
            INNER JOIN files_archive_map fam ON files.id = fam.file_id
            INNER JOIN component ON component.id=fam.component_id
            WHERE changes_id=%s
        """

    def get_binaries_for_policy_queue_by_changes(self, changesid):
        self.logger.debug("projectb::get_binaries_for_policy_queue_by_changes %s", changesid)
        query = self.policy_binaries_from_changesid_query % (changesid)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return frozenset(((row[0], row[1], row[2], row[3]) for row in result))

    is_valid_suite_name_query = """SELECT 1 FROM suite WHERE suite_name = '%s' LIMIT 1"""

    def is_valid_suite_name(self, suite):
        self.logger.debug("projectb::is_valid_suite_name %s", suite)
        query = self.is_valid_suite_name_query % (suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return len(result) > 0

    # Abuse LEFT JOINs to get 1 result if the version exists but is not in a suite
    # (makes the suite name field empty)
    check_version_query = \
        """
           SELECT 1, suite.suite_name FROM source
                  LEFT JOIN src_associations sa ON source.id = sa.source
                  LEFT JOIN suite ON sa.suite = suite.id
           WHERE source.source = '%s' AND source.version = '%s'
           LIMIT 1
        """

    def check_version(self, srcpkg, version):
        self.logger.debug("projectb::check_version %s_%s", srcpkg, version)
        query = self.check_version_query % (srcpkg, version)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if len(result) < 1:
            return (False, None)
        suite = result[0][1]
        if suite == '':
            suite = None
        return (True, suite)

    get_signed_packages_query = \
        """
            SELECT DISTINCT
                SPLIT_PART(built_using.value,' ', 1) unsigned,
                ARRAY_AGG(DISTINCT signed.source) signed
            FROM (
                SELECT source.source from binaries
                INNER JOIN source ON binaries.source = source.id
                INNER JOIN fingerprint ON source.sig_fpr = fingerprint.id
                INNER JOIN uid ON fingerprint.uid = uid.id
                INNER JOIN src_associations ON source.id = src_associations.source
                INNER JOIN suite ON src_associations.suite = suite.id
                WHERE suite.suite_name IN ('%s', '%s-debug')
                AND uid.uid = 'ftpmaster@debian.org'
                UNION
                -- hacks
                SELECT 'shim-signed'
            ) signed
            INNER JOIN (
                SELECT DISTINCT source.source, binaries_metadata.value
                FROM metadata_keys
                LEFT JOIN binaries_metadata ON metadata_keys.key_id = binaries_metadata.key_id
                LEFT JOIN binaries ON binaries.id = binaries_metadata.bin_id
                INNER JOIN source ON binaries.source = source.id
                INNER JOIN fingerprint ON source.sig_fpr = fingerprint.id
                INNER JOIN uid ON fingerprint.uid = uid.id
                INNER JOIN src_associations ON source.id = src_associations.source
                INNER JOIN suite ON src_associations.suite = suite.id
                WHERE suite.suite_name IN ('%s', '%s-debug')
                AND uid.uid='ftpmaster@debian.org'
                AND metadata_keys.key='Built-Using'
                UNION
                -- hacks
                SELECT 'shim-signed', 'shim'
            ) built_using
            ON signed.source = built_using.source
            GROUP BY 1
        """

    def get_signed_packages_for_suite(self, suite):
        self.logger.debug("projectb::get_signed_packages_for_suite %s", suite)
        query = self.get_signed_packages_query % (suite, suite, suite, suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return {row[0]: row[1] for row in result}
