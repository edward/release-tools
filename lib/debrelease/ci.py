## vim:set et ts=4 sw=4:
#
# ci.py: read lists of the CI status of packages in a
#        suite
#
# (C) Copyright 2019 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.

from __future__ import with_statement
import os.path
from yaml import safe_load

class CIFactory():
    def __init__(self, config, section):
        self.statuses_base = config.get('ci', 'directory')
        self.codename = config.get(section, 'codename')

        self.statuses = dict()
        self._load_statuses()

    def _load_statuses(self):
        """
        The input files we read are stored in the directory defined in
        the config file (section `ci', key `directory') with the name
        excuses_`codename`.yaml
        """

        filename = os.path.join(self.statuses_base, 'excuses_%s.yaml' % (self.codename))
        with open(filename, 'r') as f:
            cidata = safe_load(f)
            if cidata is None:
                cidata = {}
            for packagedata in cidata.get('sources', []):
                srcpkg = packagedata['source']
                verdict = 'UNKNOWN'
                breakages = []
                if 'policy_info' in packagedata and 'autopkgtest' in packagedata['policy_info']:
                    results = packagedata['policy_info']['autopkgtest']
                    for resultname, resultdata in results.items():
                        if resultname == 'verdict':
                            verdict = resultdata
                        else:
                            for architecture, testdata in resultdata.items():
                                if testdata[0] in ['FAIL', 'REGRESSION', 'RUNNING-REFERENCE']:
                                    breakages.append(('%s [%s]' % (resultname, architecture),
                                                     testdata[2]))
                self.statuses[srcpkg] = (verdict, breakages)
