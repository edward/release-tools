## vim:set noet ts=4 sw=4:
#
# debbindiff.py: produce debdiffs of binary packages
#
# (C) Copyright 2016-2017 Adam D. Barratt <adsb@debian.org>
#
# Based on debdiff.py:
#
# (C) 2007-2008 Philipp Kern <pkern@debian.org>
# Copyright 2011-2015 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# TODO:
# - epoch comparisons should ensure the correct epoch was used
# - dependency comparisons should ensure the binary packages
#   listed are actually built from the source package being
#   considered
# - cater for different binary packages produced by the source
#   package having different versions
# - move common regexes to variables
# - use named capture groups in regexes for clarity

import os
import gzip
import re
import time
import logging
from io import open
from cgi import escape

from subprocess import Popen, PIPE, STDOUT

from debrelease import FileNotFoundException, locate_file, strip_epoch


class DebDiffFactory():
    def __init__(self, projectb, config=None, section=None):
        self.projectb = projectb
        self.logger = logging.getLogger('debrelease.debbindiff')
        self.logger.addHandler(logging.NullHandler())
        if config is not None:
            if section:
                self.section = section
                self.pool = config.get('dak', 'pool')
                self.extra_pools = config.get('dak', 'extra_pools').split(' ')
                self.queue_directory = config.get(section, 'directory')
                self.suite = config.get(section, 'base_suite')
                self.overlay_suite = section
                self.policy_suite = config.get(section, 'policy_suite', fallback=None)
                self.diff_output = config.get(section, 'bin_diff_output')
                if not os.path.exists(self.diff_output):
                    os.mkdir(self.diff_output)
                assert os.path.isdir(self.diff_output)

    def create_debdiff(self, srcpkg, version, arch, srcver=None, olddebs=None, newdebs=None, against_overlay=False):
        overlay_version = self.projectb.get_version(self.overlay_suite, srcpkg, epoch=True)
        stable_version = self.projectb.get_version(self.suite, srcpkg, epoch=True)
        policy_version = self.projectb.get_version(self.policy_suite, srcpkg, epoch=True)
        if srcver is None:
            source_version = version
        else:
            source_version = srcver
        if against_overlay:
            base_version = overlay_version
        else:
            base_version = stable_version
        # Force overlay if not in base.
        if base_version is None:
            base_version = overlay_version
        if version == stable_version and version == source_version:
            # Diffing against the base suite won't work.
            self.logger.debug("Refusing to perform no-op diff (1)")
            return None
        if base_version == version and version == source_version:
            self.logger.debug("Refusing to perform no-op diff (2)")
            return None
        base_suite = None
        target_suite = None
        if base_version == overlay_version:
            base_suite = self.overlay_suite
            target_suite = self.policy_suite
        elif base_version == stable_version:
            base_suite = self.suite
            if version == overlay_version or not policy_version:
                target_suite = self.overlay_suite
            else:
                target_suite = self.policy_suite
        return DebDiff(
            pool=self.pool,
            extra_pools=self.extra_pools,
            queue_directory=self.queue_directory,
            srcpkg=srcpkg,
            base_version=base_version,
            target_version=source_version,
            overlay_version=overlay_version,
            stable_version=stable_version,
            projectb=self.projectb,
            olddebs=olddebs,
            newdebs=newdebs,
            architecture=arch,
            base_suite=base_suite,
            target_suite=target_suite,
        )

    def filename(self, srcpkg, version, arch, compressed, html_output):
        filever = strip_epoch(version)
        filename = "%s/%s_%s_%s.debdiff" % (self.diff_output, srcpkg, filever, arch)
        if html_output:
            filename = "%s.html" % (filename)
        if compressed:
            filename = "%s.gz" % (filename)
        return filename

    def create_diff_file(self, srcpkg, version, srcver, arch, olddebs=None, newdebs=None, against_overlay=False,
                         compressed=False, html_output=False, apply_filter=False):
        # check for an uncompressed file first
        self.logger.debug("Checking for existing diff for %s_%s/%s", srcpkg, version, arch)
        filename = self.filename(srcpkg, version, arch, False, html_output)
        if os.path.exists(filename):
            return None
        if compressed:
            filename = self.filename(srcpkg, version, arch, compressed, html_output)
            if os.path.exists(filename):
                return None
        diffobj = self.create_debdiff(srcpkg, version, arch, srcver, olddebs, newdebs, against_overlay)
        if diffobj is None:
            return None
        self.logger.debug("Need to create diff for %s_%s/%s", srcpkg, version, arch)
        diffobj.create_diff_file(filename, compressed, html_output, apply_filter)
        return filename


class DiffFailedException(Exception):
    def __init__(self, base_files, target_files, error):
        self.base_files, self.target_files, self.error = \
            base_files, target_files, error
        super().__init__(
            "DebBinDiff(%s, %s) failed:\n%s" % (self.base_files,
                                                self.target_files, self.error))


def classify(line, html=False):
    if line in ['[The following lists of changes regard files as different if they have',
                'different names, permissions or owners.]']:
        linetype = 'preamble'
    elif re.search(r'^-+$', line):
        if html:
            linetype = 'blank'
        else:
            linetype = 'header'
    elif (line == 'File lists identical on package level (after any substitutions)' or
          line.startswith('No differences were encountered between ')):
        linetype = 'nodiff'
    elif line == '':
        linetype = 'blank'
    elif (line.startswith('Files only in first set of .debs') or
          line.startswith('New files in second set of .debs') or
          line.startswith('Warning: these package names were in the ') or
          line.startswith('Files moved or copied from ') or
          re.search(r'^[^ ]+ files( of package [^ ]+)?: lines which differ', line)):
        linetype = 'header'
    elif line.startswith('From package') or line.startswith('To package'):
        linetype = 'subheader'
    else:
        linetype = 'detail'

    return linetype


class DebDiff():

    def __init__(self, srcpkg, target_version, base_version,
                 pool, extra_pools=[],
                 projectb=None, olddebs=None, newdebs=None,
                 architecture=None,
                 base_suite=None, target_suite=None,
                 queue_directory=None, stable_version=None, overlay_version=None):
        self.projectb = projectb
        self.logger = logging.getLogger('debrelease.debbindiff')
        self.logger.addHandler(logging.NullHandler())
        self.queue_directory = queue_directory
        self.pool = pool
        self.extra_pools = extra_pools
        self.base_version = base_version
        self.base_suite = base_suite
        self.base_debug_suite = "%s-debug" % (base_suite, )
        self.target_version = target_version
        self.target_suite = target_suite
        self.target_debug_suite = "%s-debug" % (target_suite, )
        self.srcpkg = srcpkg
        self.architecture = architecture
        self.olddebs = []
        self.html_output = False
        self.apply_filter = False
        if olddebs is None:
            self.logger.debug("Determining base binaries for %s_%s in %s", srcpkg, base_version, self.base_suite)
            olddebs = [pkg.filename for pkg in self.projectb.get_binaries_for_version(srcpkg, base_version)
                       if pkg.arch_string == architecture and (base_suite is None or pkg.suite in [self.base_suite, self.base_debug_suite])]
            self.logger.debug("Got: %s", " ".join(olddebs))
        if newdebs is None:
            self.logger.debug("Determining target binaries for %s_%s in %s", srcpkg, target_version, self.target_suite)
            newdebs = [pkg.filename for pkg in self.projectb.get_binaries_for_version(srcpkg, target_version)
                       if pkg.arch_string == architecture and (target_suite is None or pkg.suite in [self.target_suite, self.target_debug_suite])]
            self.logger.debug("Got: %s", " ".join(newdebs))
        self.logger.debug("Locating binary package files")
        self.olddebs = [locate_file(filename,
                                    self.queue_directory,
                                    [self.pool] + self.extra_pools) for filename in sorted(olddebs)]
        self.newdebs = [locate_file(filename,
                                    self.queue_directory,
                                    [self.pool] + self.extra_pools) for filename in sorted(newdebs)]
        # This is a hack that assumes that:
        # a) all binary packages built from the source package share
        #    the same version
        # b) the first binary package filename correctly indicates its
        #    version
        #
        # However, it will work in the majority of cases
        if self.olddebs:
            self.base_bin_version = self.olddebs[0].split('/')[-1].split('_')[1]
        else:
            self.base_bin_version = '(None)'
        if self.newdebs:
            self.target_bin_version = self.newdebs[0].split('/')[-1].split('_')[1]
        else:
            self.target_bin_version = '(None)'
        self.version_info = {}
        if stable_version is not None:
            self.version_info['stable'] = stable_version
        if overlay_version is not None:
            self.version_info['overlay'] = overlay_version

        # confirm that all files exist
        for file in self.olddebs + self.newdebs:
            if not os.path.exists(file):
                raise FileNotFoundException(file)

    def _invoke_debdiff(self):
        if not self.olddebs or not self.newdebs:
            return None

        extra_info = ''
        if self.html_output and self.apply_filter:
            extra_info = " (HTML output, filtered)"
        elif self.html_output:
            extra_info = " (HTML output)"
        elif self.apply_filter:
            extra_info = " (filtered)"
        self.logger.info("Generating debdiff for %s_%s/%s vs %s_%s/%s%s",
                         self.srcpkg, self.target_version, self.architecture,
                         self.srcpkg, self.base_version, self.architecture,
                         extra_info)
        args = ['debdiff', '--controlfiles=ALL', '--show-moved', '--from']
        args.extend(self.olddebs + ['--to'] + self.newdebs)

        diff = Popen(args, stdout=PIPE, stderr=STDOUT, close_fds=True)
        output = diff.communicate()
        if diff.returncode not in [0, 1]:
            raise DiffFailedException(' '.join(self.olddebs), ' '.join(self.newdebs),
                                      output[0].strip())
        return output[0]

    def __str__(self):
        return self.diff

    def write_to_file(self, file):
        if self.html_output:
            if self.htmldiff:
                file.write(self.htmldiff)
        else:
            if self.diff:
                file.write(self.diff)

    @property
    def diff(self):
        if self.apply_filter:
            return self.filtereddiff
        else:
            return self.fulldiff

    @property
    def fulldiff(self):
        if not hasattr(self, '_diff'):
            self._diff = self._invoke_debdiff()
        return self._diff

    @property
    def filtereddiff(self):
        if not hasattr(self, '_filtereddiff'):
            self._filtereddiff = self._apply_filter()
        return self._filtereddiff

    def _apply_filter(self):
        if not self.fulldiff:
            return None

        self.logger.debug("Applying diff filter")
        (_diff, section, package, extra, detail) = ([], '', '', [], [])
        base_version_noepoch = strip_epoch(self.base_version)
        _base_version = re.escape(base_version_noepoch)
        base_upstream_version = self.base_version.split('-')[0]
        base_upstream_version_noepoch = strip_epoch(base_upstream_version)
        _base_upstream_version = re.escape(base_upstream_version_noepoch).replace(r'\+dfsg', r'(?:\+dfsg)?')
        _base_bin_version = re.escape(self.base_bin_version)
        base_bin_version_noepoch = strip_epoch(self.base_bin_version)
        _base_is_binnmu = re.search(r'\+b\d+$', self.base_bin_version)
        target_version_noepoch = strip_epoch(self.target_version)
        _target_version = re.escape(target_version_noepoch)
        target_upstream_version = self.target_version.split('-')[0]
        target_upstream_version_noepoch = strip_epoch(target_upstream_version)
        _target_upstream_version = re.escape(target_upstream_version_noepoch).replace(r'\+dfsg', r'(?:\+dfsg)?')
        _target_bin_version = re.escape(self.target_bin_version)
        target_bin_version_noepoch = strip_epoch(self.target_bin_version)
        _target_is_binnmu = re.search(r'\+b\d+$', self.target_bin_version)
        for line in self.fulldiff.decode().split('\n'):
            classification = classify(line)
            if classification in ('preamble', 'nodiff', 'blank'):
                continue
            if classification == 'header':
                if re.search(r'^-+$', line):
                    extra = [line]
                else:
                    if section != '' and detail:
                        _diff.extend([section] + extra + [''] + detail + [''])
                        detail = []
                    section = line
                    if section.startswith('Control files of'):
                        package = section.split()[4].replace(':', '')
                    elif section.startswith('Files only in') or section.startswith('New files in'):
                        package = section.split()[-1]
                    else:
                        package = ''
                    extra = []
            elif classification in ('subheader', 'detail'):
                if _base_is_binnmu and not _target_is_binnmu and \
                   (line.startswith('[-Source:') or re.search(r'^Source: .* \[-\(.*\)-\]$', line)):
                    # binNMU to source upload, dropping indicator
                    continue
                elif not _base_is_binnmu and _target_is_binnmu and line.startswith('{+Source:'):
                    # source upload to binNMU, adding indicator
                    continue
                elif line.startswith('Source:'):
                    if not _base_is_binnmu and _target_is_binnmu:
                        # source upload to binNMU, adding indicator
                        if re.search(r'^Source: %s \{\+\((\d+:)?%s\)\+\}' %
                           (self.srcpkg, _base_version), line) is not None:
                            continue
                    else:
                        # source uploads of a binary package with a different
                        # name from the source package
                        match = re.search(r'^Source: %s \[-\((\d+:)?%s\)-\] \{\+\((\d+:)?%s\)\+\}' %
                                          (self.srcpkg, _base_version, _target_version), line)
                        if match is not None and match.group(1) == match.group(2):
                            continue
                    # unexpected change
                    detail.append(line)
                elif (line.startswith('Version:') or section.startswith('Shlibs files of') or
                      section.startswith('Symbols files of')):
                    match = re.search(r'\[-(\d+:)?(?:%s|%s|%s)(~?\)?,?)-\] \{\+(\d+:)?(?:%s|%s|%s)(~?\)?,?)\+\}' %
                                      (_base_version, _base_upstream_version, _base_bin_version,
                                       _target_version, _target_upstream_version, _target_bin_version), line)
                    # groups are:
                    #    base epoch, base trailer, target epoch, target trailer
                    if match is not None and match.group(1) == match.group(3) and \
                       match.group(2) == match.group(4):
                        continue
                    else:
                        detail.append(line)
                elif re.search(r'^(?:(?:Pre-)?Depends|Suggests|Recommends|Conflicts|Breaks|Enhances|Replaces|Provides):', line):
                    if re.search(r'\[-[a-z0-9+.-]+(:\w+)?( \((>[=>]|=|<[=<]) (\d+:)?(.*?)(~?\)?,?))?-\]( [^{]|$)', line):
                        # dependency removed
                        detail.append(line)
                    elif re.search(r'(^[\w-]+: |[^]] )\{\+([a-z0-9+.-]+(:\w+)?)?( ?\((>[=>]|=|<[=<]) (\d+:)?(.*?)(~?\)?,?))?\+\}', line):
                        # dependency added
                        detail.append(line)
                    elif re.search(r'\[-(\d+:)?(.*?)(~?\)?,?)-\] \{\+(\d+:)?(.*?)(~?\)?,?)\+\}', line):
                        # These checks are overly-broad for Provides, where "=" is the only permitted
                        # operator. However, a change of operator will still be flagged, and relationships
                        # using other operators were already broken.
                        #
                        # groups are:
                        #    base epoch, base version, base trailer
                        #    target epoch, target version, target trailer
                        for match in re.finditer(r'\[-(\d+:)?(.*?)(~?\)?,?)-\] \{\+(\d+:)?(.*?)(~?\)?,?)\+\}', line):
                            version_match = re.search(r'\[-(\d+:)?(%s|%s|%s)(~?\)?,?)-\] \{\+(\d+:)?(%s|%s|%s)(~?\)?,?)\+\}' %
                                                      (_base_version, _base_upstream_version, _base_bin_version,
                                                       _target_version, _target_upstream_version, _target_bin_version), line)
                            if version_match and \
                               match.group(1) == match.group(4) and \
                               match.group(3) == match.group(6):
                                continue
                            else:
                                # dependency version changed unexpectedly
                                detail.append(line)
                                break
                    else:
                        detail.append(line)
                elif section.startswith('Files only in') or section.startswith('New files in'):
                    if (package.endswith('-dbg') or package.endswith('-dbgsym')) and \
                       re.search(r'^-rw-r--r-- +root/root +/usr/lib/debug/\.build-id/[0-9a-f]{2}/(?:[0-9a-f]{38,40}|[0-9a-f]{14})\.debug', line):
                        continue
                    elif (section.startswith('Files only in') and
                          _base_is_binnmu and not _target_is_binnmu and
                          line.endswith('/changelog.Debian.%s.gz' % (self.architecture))):
                        # binNMU to source upload; dropping binNMU changelog
                        continue
                    elif (section.startswith('New files in') and
                          _target_is_binnmu and not _base_is_binnmu and
                          line.endswith('/changelog.Debian.%s.gz' % (self.architecture))):
                        # source upload to binNMU; adding binNMU changelog
                        continue
                    else:
                        detail.append(line)
                elif line.startswith('Installed-Size:'):
                    match = re.search(r'\[-(.*)-\] \{\+(.*)\+\}', line)
                    if match and (int(match.group(2)) - int(match.group(1))) / int(match.group(1)) <= 0.1:
                        continue
                    else:
                        detail.append(line)
                elif line.startswith('Build-Ids:') and (package.endswith('-dbg') or package.endswith('-dbgsym')):
                    # change of build IDs for a debugging package; expected
                    continue
                elif re.search(r'^(?:(?:Pre|Post)(?:inst|rm)|Triggers) files(?: of package [^ ]+)?: lines which differ', section):
                    # groups are:
                    #    original dh helper name, version
                    #    new dh helper name, version
                    match = re.search(r'^# (?:Automatically|Triggers) added by \[-(dh_.+?)/?(.+?)-\] \{\+(dh_.+?)/?(.+?)\+\}$', line)
                    if match and match.group(1) == match.group(3):
                        continue
                    else:
                        detail.append(line)
                else:
                    detail.append(line)
        if section != '' and detail:
            _diff.extend([section] + extra + [''] + detail + [''])

        return '\n'.join(_diff).encode('utf-8')

    @property
    def htmldiff(self):
        if not hasattr(self, '_htmldiff'):
            self._htmldiff = self._generate_html_diff()
        return self._htmldiff

    def _generate_html_diff(self):
        self.logger.debug("Generating HTML output")
        _diff = []
        _toc = []
        if self.diff:
            lines = self.diff.decode().split('\n')
        else:
            lines = []
        for line in lines:
            classification = classify(line, html=True)

            if classification in ('blank', 'preamble', 'nodiff'):
                continue

            line = escape(line)

            if classification == 'header':
                line = re.sub(r' \(wdiff format\)$', '', line)

                anchor = re.sub(r' ', r'_', re.sub(r'\W', r'', line.lower()))
                _toc.append((anchor, line))
                line = '<h2 class="debdiff-header" id="%s">%s</h2>' % (anchor, line)
            elif classification == 'subheader':
                line = '<h3 class="debdiff-header">%s</h3>' % (line)
            else:
                line = re.sub(r'\[-(.+?)-\]', r'<span class="wdiff-removal">\1</span>', line)
                line = re.sub(r'\{\+(.+?)\+\}', r'<span class="wdiff-addition">\1</span>', line)
                line = '<div class="debdiff-content"><span class="debdiff-content">%s</span></div>' % (line)

            _diff.append(line)
        if _toc:
            toc = ['<div class="debdiff-toc">', '<ul class="debdiff-toc">']
            for anchor, text in _toc:
                toc.append('<li><a href="#%s">%s</a></li>' % (anchor, text))
            toc.append('</ul>')
            toc.append('</div>')
        else:
            toc = []
            _diff.append('<div class="boring">After applying filtering, no changes remain</div>')
        _diff.append('<div id="footer"><p><strong>Timestamp:</strong> %s</p>' %
                     (time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime())))
        _diff.append('<p><strong>Generated by:</strong> ')
        _diff.append('<a href="https://salsa.debian.org/release-team/release-tools/tree/master/queue-viewer" rel="nofollow">queue-viewer</a>')
        _diff.append('</p></div>')
        _diff.append('</div></body></html>')
        return '\n'.join(toc + _diff).encode('utf-8')

    def _print_header(self, f):
        if self.html_output:
            self._print_html_header(f)
        else:
            self._print_plain_header(f)

    def _print_plain_header(self, f):
        if 'stable' in self.version_info:
            f.write(("Version in base suite: %s\n" % self.version_info['stable']).encode('ascii'))
        if 'overlay' in self.version_info:
            if self.version_info['overlay'] is None:
                version = "(not present)"
            else:
                version = self.version_info['overlay']
            f.write(("Version in overlay suite: %s\n" % version).encode('ascii'))
        f.write(b"\n")
        f.write(("Base version: %s_%s\n" % (self.srcpkg, self.base_version)).encode('ascii'))
        f.write(("Target version: %s_%s\n" % (self.srcpkg, self.target_version)).encode('ascii'))
        if self.olddebs:
            f.write(("Base files: %s\n" % (' '.join(sorted(os.path.basename(file) for file in self.olddebs)))).encode('ascii'))
        else:
            f.write(("Base files: (none)\n").encode('ascii'))
        if self.newdebs:
            f.write(("Target files: %s\n" % (' '.join(sorted(os.path.basename(file) for file in self.newdebs)))).encode('ascii'))
        else:
            f.write(("Target files: (none)\n").encode('ascii'))
        f.write(b"\n")

    def _html_version_wrap(self, label, info):
        return '<p class="version"><span class="versionlabel">%s</span>: <span class="versioninfo">%s</span></p>\n' % (label, info)

    def _print_html_header(self, f):
        f.write(('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">\n').encode('ascii'))
        f.write(('<html xmlns="http://www.w3.org/1999/xhtml" lang="en">\n').encode('ascii'))
        f.write(('<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>\n').encode('ascii'))
        f.write(('<link rel="stylesheet" href="../gfx/debian.css"/><link rel="stylesheet" href="../gfx/styles.css"/>\n').encode('ascii'))
        f.write(('<title>Debian binary debdiff for %s</title></head><body>\n' % (self.srcpkg)).encode('ascii'))
        f.write(('<div id="header">\n').encode('ascii'))
        f.write(('      <div id="upperheader">\n').encode('ascii'))
        f.write(('        <div id="logo">\n').encode('ascii'))
        f.write(('          <a href="//www.debian.org/"><img src="//www.debian.org/Pics/openlogo-50.png" alt="Debian" width="50" height="61"/></a>\n').encode('ascii'))
        f.write(('        </div>\n').encode('ascii'))
        f.write(('        <p class="section"><a href="/">Release Team</a></p>\n').encode('ascii'))
        f.write(('      </div>\n').encode('ascii'))
        f.write(('      <div id="navbar">\n').encode('ascii'))
        f.write(('        <ul>\n').encode('ascii'))
        f.write(('          <li><a href="//www.debian.org/intro/about">About Debian</a></li>\n').encode('ascii'))
        f.write(('          <li><a href="//www.debian.org/distrib/">Getting Debian</a></li>\n').encode('ascii'))
        f.write(('          <li><a href="//www.debian.org/support">Support</a></li>\n').encode('ascii'))
        f.write(('          <li><a href="//www.debian.org/devel/">Developers\'&#160;Corner</a></li>\n').encode('ascii'))
        f.write(('        </ul>\n').encode('ascii'))
        f.write(('      </div>\n').encode('ascii'))
        f.write(('      <p id="breadcrumbs"><a href="/">release.debian.org</a> / <a href="/proposed-updates/">proposed-updates</a> / <a href="/proposed-updates/stable.html">stable</a> / debdiff</p>\n').encode('ascii'))
        f.write(('    </div>\n').encode('ascii'))
        f.write(('    <div id="content">\n').encode('ascii'))

        if 'stable' in self.version_info:
            f.write((self._html_version_wrap("Version in base suite", self.version_info['stable'])).encode('ascii'))
        if 'overlay' in self.version_info:
            if self.version_info['overlay'] is None:
                version = "(not present)"
            else:
                version = self.version_info['overlay']
            f.write((self._html_version_wrap("Version in overlay suite", version)).encode('ascii'))
        f.write(b"\n")
        f.write((self._html_version_wrap("Base version", "%s_%s" % (self.srcpkg, self.base_version))).encode('ascii'))
        f.write((self._html_version_wrap("Target version", "%s_%s" % (self.srcpkg, self.target_version))).encode('ascii'))
        if self.olddebs:
            f.write((self._html_version_wrap("Base files", (' '.join(sorted(os.path.basename(file) for file in self.olddebs))))).encode('ascii'))
        else:
            f.write((self._html_version_wrap("Base files", "(None)")).encode('ascii'))
        if self.newdebs:
            f.write((self._html_version_wrap("Target files", (' '.join(sorted(os.path.basename(file) for file in self.newdebs))))).encode('ascii'))
        else:
            f.write((self._html_version_wrap("Target files", "(None)")).encode('ascii'))
        f.write(b"\n")

    def create_diff_file(self, filename, compressed=False, html_output=False, apply_filter=False):
        self.logger.debug("Outputting diff to file")
        self.html_output = html_output
        self.apply_filter = apply_filter
        if compressed:
            f = gzip.open(filename, 'w')
        else:
            f = open(filename, 'wb')
        try:
            self._print_header(f)
            self.write_to_file(f)
        finally:
            f.close()
