## vim:set noet ts=4 sw=4:
#
# (C) 2007-2008 Philipp Kern <pkern@debian.org>
# (C) 2009-2019 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.

import io
import os.path
import os
import re
import sys
import logging
import subprocess
from collections import defaultdict
from shutil import copyfile
from six.moves import cPickle


from debian.deb822 import Deb822

# in minutes
IGNORABLE_TIME_DELTA = 120

SPECIAL_ARCHES = frozenset(['all', 'source'])

logging.getLogger('debrelease').addHandler(logging.NullHandler())


class FileNotFoundException(Exception):
    def __init__(self, filename):
        self.filename = filename
        super().__init__("File not found: %s" % (self.filename,))


def tree():
    return defaultdict(tree)


def strip_epoch(version):
    """Strips away the epoch of a version number because we cannot rely on its
    presence in various places."""
    if ':' in version:
        return version.split(':', 1)[1]
    else:
        return version


def normalize_path(path):
    """Normalizes the passed path to the path of the script.  This function is
    used to complete relative directories specified in the configuration
    file."""
    return os.path.normpath(
        os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), path))


def extract_epoch(version):
    """Extracts and returns the epoch of a version number.  If the version does
    not contain an epoch, returns None."""
    if ':' in version:
        return "%s:" % (version.split(':', 1)[0])
    else:
        return None


def create_symlinks(dsc_file, link_dir, pools, projectb):
    # caller is responsible for creating and cleaning up
    # "link_dir"
    with open(dsc_file, encoding='utf-8') as dsc_handler:
        dsc = Deb822(dsc_handler)
        os.symlink(dsc_file,
                   os.path.join(link_dir, os.path.basename(dsc_file)))
        dsc_dir = os.path.dirname(dsc_file)
        files = frozenset(dsc['Files'].split('\n'))
        for files_entry in files:
            if files_entry == '':
                continue
            file = files_entry.rsplit(' ', 1)[1]
            source_file = os.path.join(dsc_dir, file)
            link = os.path.join(link_dir, file)
            if not os.path.exists(link):
                if not os.path.exists(source_file):
                    for pool in pools:
                        _source_file = '%s/%s' % (pool,
                                                  projectb.get_pool_file(file))
                        if os.path.exists(_source_file):
                            source_file = _source_file
                            break
                os.symlink(source_file, link)


def copy_source_package(dsc_file, dest_dir, pools, projectb):
    # caller is responsible for creating and cleaning up
    # "dest_dir"
    with open(dsc_file, encoding='utf-8') as dsc_handler:
        dsc = Deb822(dsc_handler)
        copyfile(dsc_file,
                 os.path.join(dest_dir, os.path.basename(dsc_file)))
        dsc_dir = os.path.dirname(dsc_file)
        files = frozenset(dsc['Files'].split('\n'))
        for files_entry in files:
            if files_entry == '':
                continue
            file = files_entry.rsplit(' ', 1)[1]
            source_file = os.path.join(dsc_dir, file)
            dest = os.path.join(dest_dir, file)
            if not os.path.exists(dest):
                if not os.path.exists(source_file):
                    for pool in pools:
                        _source_file = '%s/%s' % (pool,
                                                  projectb.get_pool_file(file))
                        if os.path.exists(_source_file):
                            source_file = _source_file
                            break
                copyfile(source_file, dest)


def locate_file(filename, queue_directory, pools):
    if filename.startswith('/'):
        pass
    elif filename.find('/') == -1 and queue_directory:
        filename = "%s/%s" % (queue_directory, filename)
    else:
        for pool in pools:
            _filename = "%s/%s" % (pool, filename)
            if os.path.exists(_filename):
                filename = _filename
                break
    return filename


class RestrictedUnpickler(cPickle.Unpickler):
    def find_class(self, module, name):
        raise cPickle.UnpicklingError("global '%s.%s' is forbidden" %
                                      (module, name))


def safe_pickle_load(content):
    if isinstance(content, io.BufferedIOBase):
        return RestrictedUnpickler(io.BytesIO(content.read())).load()
    else:
        return RestrictedUnpickler(io.BytesIO(content)).load()


def ordinal_tail(number):
    tails = {1: "st", 2: "nd", 3: "rd", 11: "th", 12: "th", 13: "th"}
    return tails.get(number, tails.get(number % 10, "th"))


def number_to_word(number):
    mapping = {1: "one", 2: "two", 3: "three", 4: "four", 5: "five", 6: "six",
               7: "seven", 8: "eight", 9: "nine", 10: "ten", 11: "eleven",
               12: "twelve", 13: "thirteen", 15: "fifteen", 18: "eighteen",
               20: "twenty", 30: "thirty", 40: "forty", 50: "fifty",
               80: "eighty"}
    if number in mapping.keys():
        return mapping[number]
    elif 14 <= number <= 19:
        return "%steen" % (mapping[number % 10])
    else:
        tens = mapping.get(10*(number // 10), "%sty" % (mapping[number // 10]))
        units = mapping.get(number % 10, "")
        return "%s-%s" % (tens, units) if units else tens


def ordinal_stem(number):
    stems = {1: "fir", 2: "seco", 3: "thi", 5: "fif", 8: "eigh", 9: "nin", 12: "twelf"}
    if number in stems.keys():
        stem = stems[number]
    elif number >= 20:
        stem = number_to_word(10*(number // 10))
    else:
        stem = number_to_word(number)
    if stem.endswith("e"):
        stem = stem[:-1]
    elif stem.endswith("y") and number % 10 == 0:
        stem = stem[:-1] + "ie"
    return stem


def ordinal_words(number):
    stem = ordinal_stem(number)
    if number >= 20:
        if number % 10 != 0:
            stem = "%s-%s" % (stem, ordinal_stem(number % 10))
        tail = ordinal_tail(number % 10)
    else:
        tail = ordinal_tail(number)
    result = "%s%s" % (stem, tail)
    result = result.replace("rdt", "rt")
    return result


def ordinal(number):
    return "%s%s" % (number, ordinal_tail(number))


def mangled_signed_version(version, package=''):
    # MANGLED_VERSION="$(echo $VERSION | sed -r 's/-/\+/;s/\+(b[[:digit:]]+)$/.\1/')"
    mangled_version = re.sub(r"-", r"+", re.sub(r"\+(b\d+)$", r"\.\1", version))
    if package in ['grub2', 'shim']:
        mangled_version = "1+%s" % (mangled_version)
    return mangled_version


def send_mail(message):
    # LSB-specified parameters:
    # -t: read addresses out of the passed-in headers
    # -i: ignore dots alone on lines
    # -oep: errors onto stderr
    proc = subprocess.Popen(['/usr/sbin/sendmail', '-t', '-i', '-oep'],
                            stdin=subprocess.PIPE, close_fds=True)
    proc.stdin.write(message.as_string().encode('utf-8', 'backslashreplace'))
    proc.stdin.close()
    if proc.wait() != 0:
        raise Exception("sending mail failed: %d" % proc.returncode)
